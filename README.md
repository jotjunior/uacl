Jot! UACL
=========

Introdução
----------
Módulo do Zend Framework 2 responsável por autenticar e definir permissões de 
acesso aos usuários.

O módulo utiliza os componentes Zend\Auth, Zend\Acl e Zend\Cache para garantir
uma melhor performance e mais segurança no acesso aos dados.

Toda a autenticação é feita usando o método Oauth2 (que será baixado autometicamente
pelo composer).

Instalação
----------

Usando Composer (recomendado)
-----------------------------
### Passo 1
Adicione as seguintes linhas no seu composer.json:

    {
        "require": {
            "jotjunior/uacl": "dev-master"
        },
        "repositories": [
            {  
                "type": "git",
                "url": "git@bitbucket.org:jotjunior/uacl.git"
            }
        ]
    }
   

### Passo 2
Use o comando `php composer.phar install`

### Passo 3
Crie as tabelas de banco de dados necessárias ao módulo. Elas se encontram no 
diretório `dist` e possui versões para MySQL e PosgreSQL.

### Passo 4
Acesse o seguinte endereço: `http://seudominio/create-admin`

Esse comando realizará as seguintes ações:

1. Criará uma Role GUEST e uma Role ADMIN
2. Criará um idioma padrão (no caso, Português do Brasil)
3. Criará um usuário administrador
4. Registrará todos os Resources e Privileges da UACL para o administrador
