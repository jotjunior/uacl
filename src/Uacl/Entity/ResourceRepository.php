<?php

namespace Uacl\Entity;

use Doctrine\ORM\EntityRepository;

class ResourceRepository extends EntityRepository
{

    public function getList()
    {
        $result = array();

        foreach ($this->findAll() as $resource) {
            $result[$resource->getId()] = $resource->getName();
        }

        return $result;
    }

    public function getPermissionMap()
    {
        $result = array();

        $privilegeRepository = $this->getEntityManager()
                ->getRepository('Uacl\Entity\Privilege');

        foreach ($this->findBy(array(), array('name' => 'ASC')) as $resource) {
            $result[] = array(
                'id' => $resource->getId(),
                'name' => $resource->getName(),
                'label' => $resource->getLabel(),
                'privileges' => $privilegeRepository->listPrivilegesByResource($resource)
            );
        }

        return $result;
    }

}
