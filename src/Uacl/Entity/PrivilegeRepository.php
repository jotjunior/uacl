<?php

namespace Uacl\Entity;

use Doctrine\ORM\EntityRepository;

class PrivilegeRepository extends EntityRepository
{

    public function getList()
    {
        $result = array();

        foreach ($this->findAll() as $privilege) {
            $result[$privilege->getId()] = array(
                'role' => $privilege->getRole()->getName(),
                'resource' => $privilege->getResource()->getName(),
                'action' => $privilege->getAction()
            );
        }

        return $result;
    }

    public function listPrivilegesByResource($resource)
    {

        $result = array();

        $privileges = $this->findBy(array('resource' => $resource), array('action' => 'ASC'));

        foreach ($privileges as $privilege) {
            $result[] = array(
                'id' => $privilege->getId(),
                'role' => $privilege->getRole()->toArray(),
                'action' => $privilege->getAction(),
                'label' => $privilege->getLabel(),
            );
        }

        return $result;
    }

}
