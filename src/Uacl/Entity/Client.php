<?php

namespace Uacl\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UaclClients
 *
 * @ORM\Table(name="uacl_clients", indexes={@ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class Client
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client_key", type="string", length=40, nullable=false)
     */
    private $clientKey;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="client_secret", type="string", length=80, nullable=false)
     */
    private $clientSecret;

    /**
     * @var string
     *
     * @ORM\Column(name="redirect_uri", type="string", length=2000, nullable=false)
     */
    private $redirectUri;

    /**
     * @var string
     *
     * @ORM\Column(name="grant_types", type="string", length=80, nullable=true)
     */
    private $grantTypes;

    /**
     * @var \UaclUsers
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'redirectUri' => $this->getRedirectUri(),
            'grantTypes' => $this->getGrantTypes(),
            'name' => $this->getName(),
            'clientKey' => $this->getClientKey(),
            'clientSecret' => $this->getClientSecret(),
        );
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRedirectUri()
    {
        return $this->redirectUri;
    }

    public function getGrantTypes()
    {
        return $this->grantTypes;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setRedirectUri($redirectUri)
    {
        $this->redirectUri = $redirectUri;
        return $this;
    }

    public function setGrantTypes($grantTypes)
    {
        $this->grantTypes = $grantTypes;
        return $this;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getClientKey()
    {
        return $this->clientKey;
    }

    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    public function setClientKey($clientKey)
    {
        $this->clientKey = $clientKey;
        return $this;
    }

    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

}
