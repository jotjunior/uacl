<?php

namespace Uacl\Entity;

use Doctrine\ORM\EntityRepository;

class RoleRepository extends EntityRepository
{

    public function getList()
    {
        $result = array();

        foreach ($this->findAll() as $role) {
            $result[$role->getId()] = array(
                'name' => $role->getName(),
                'parent' => ($role->getParent()) ? $role->getParent()->getName() : null,
                'admin' => $role->getAdmin()
            );
        }

        return $result;
    }

}
