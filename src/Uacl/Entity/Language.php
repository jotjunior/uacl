<?php

namespace Uacl\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AclLanguages
 *
 * @ORM\Table(name="uacl_languages")
 * @ORM\Entity
 */
class Language
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="native_name", type="string", length=100, nullable=false)
     */
    private $nativeName;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     */
    private $locale;
    
    public function toArray() {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'nativeName' => $this->getNativeName(),
            'locale' => $this->getLocale(),
        );
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getNativeName()
    {
        return $this->nativeName;
    }

    public function setNativeName($nativeName)
    {
        $this->nativeName = $nativeName;
        return $this;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

}
