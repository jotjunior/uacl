<?php

namespace Uacl\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UaclAccessTokens
 *
 * @ORM\Table(name="uacl_access_tokens", indexes={@ORM\Index(name="client_id", columns={"client_id"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class AccessToken
{

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=100, nullable=false)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=50, nullable=true)
     */
    private $ip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires", type="datetime", nullable=false)
     */
    private $expires = 'CURRENT_TIMESTAMP';

    /**
     * @var \UaclClients
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \UaclUsers
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'ip' => $this->getIp(),
            'expires' => $this->getExpires()->format('d/m/Y H:i:s'),
            'created' => $this->getExpires()->sub(new \DateInterval('PT2H'))->format('d/m/Y H:i:s'),
            'user' => [
                'id' => $this->getUser()->getId(),
                'firstName' => $this->getUser()->getFirstName(),
                'lastName' => $this->getUser()->getLastName(),
            ],
            'client' => [
                'id' => $this->getClient()->getId(),
                'name' => $this->getClient()->getName()
            ]
        ];

    }

    public function getId()
    {
        return $this->id;
    }

    public function getExpires()
    {
        return $this->expires;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setExpires($expires)
    {
        $this->expires = $expires;
        return $this;
    }

    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

}
