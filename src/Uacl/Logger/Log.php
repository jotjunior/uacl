<?php

namespace Uacl\Logger;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Log\Logger;
use Zend\Log\Writer;

class Log implements FactoryInterface
{

    const log_dir = 'data/log';

    protected $logger;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // tentando criar o diretório do log
        @mkdir(self::log_dir, 0777);

        $stream = fopen(self::log_dir . '/nobody.log', 'a');

        $writer = new Writer\Stream($stream);

        // instanciando o logger
        $this->logger = new Logger;

        // injetando o writer
        $this->logger->addWriter($writer);
        
        return $this->logger;
    }

}
