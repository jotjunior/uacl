<?php

namespace Uacl\Auth\Storage;

use OAuth2\Storage\AccessTokenInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

class AccessTokenStorage implements AccessTokenInterface, FactoryInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        return $this;
    }

    /**
     * Look up the supplied oauth_token from storage.
     *
     * We need to retrieve access token data as we create and verify tokens.
     *
     * @param $oauth_token
     * oauth_token to be check with.
     *
     * @return array|bool
     * An associative array as below, and return NULL if the supplied oauth_token
     * is invalid:
     * - client_id: Stored client identifier.
     * - expires: Stored expiration in unix timestamp.
     * - scope: (optional) Stored scope values in space-separated string.
     *
     * @ingroup oauth2_section_7
     */
    public function getAccessToken($oauth_token)
    {
        $accessToken = $this->em
                ->getRepository('Uacl\Entity\AccessToken')
                ->find($oauth_token);

        if ($accessToken) {
            return array(
                'access_token' => $accessToken->getId(),
                'client_id' => $accessToken->getClient()->getClientKey(),
                'user_id' => ($accessToken->getUser()) ? $accessToken->getUser()->getUsername() : null,
                'expires' => $accessToken->getExpires()->getTimestamp(),
                'user' => $accessToken->getUser()->toArray()
            );
        }

        return false;
    }

    /**
     * Store the supplied access token values to storage.
     *
     * We need to store access token data as we create and verify tokens.
     *
     * @param $oauth_token     oauth_token to be stored.
     * @param $client_id       Client identifier to be stored.
     * @param $user_id         User identifier to be stored.
     * @param int $expires     Expiration to be stored as a Unix timestamp.
     * @param string $scope    (optional) Scopes to be stored in space-separated string.
     *
     * @ingroup oauth2_section_4
     */
    public function setAccessToken($oauth_token, $client_id, $user_id, $expires, $scope = null)
    {

        $accessToken = new \Uacl\Entity\AccessToken();
        $accessToken->setId($oauth_token)
                ->setClient($this->em->getRepository('Uacl\Entity\Client')->findOneBy(array('clientKey' => $client_id)))
                ->setUser($this->em->getRepository('Uacl\Entity\User')->findOneBy(array('username' => $user_id)))
                ->setIp($_SERVER['REMOTE_ADDR'])
                ->setExpires(new \DateTime(date("Y-m-d H:i:s", $expires)));

        $this->em->persist($accessToken);
        $this->em->flush();

        return array(
            'access_token' => $accessToken->getId(),
            'client_id' => $accessToken->getClient()->getClientKey(),
            'user_id' => ($accessToken->getUser()) ? $accessToken->getUser()->getUsername() : null,
            'expires' => $accessToken->getExpires()->getTimestamp(),
            'ip' => $accessToken->getIp()
        );
    }

}
