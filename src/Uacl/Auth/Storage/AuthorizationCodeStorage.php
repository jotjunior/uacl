<?php

namespace Uacl\Auth\Storage;

use OAuth2\Storage\AuthorizationCodeInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

class AuthorizationCodeStorage implements AuthorizationCodeInterface, FactoryInterface
{

    /**
     * @var Doctrine\ORM\EntityManager 
     */
    protected $em;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        return $this;
    }

    /**
     * Fetch authorization code data (probably the most common grant type).
     *
     * Retrieve the stored data for the given authorization code.
     *
     * Required for OAuth2::GRANT_TYPE_AUTH_CODE.
     *
     * @param $code
     * Authorization code to be check with.
     *
     * @return
     * An associative array as below, and NULL if the code is invalid
     * @code
     * return array(
     *     "client_id"    => CLIENT_ID,      // REQUIRED Stored client identifier
     *     "user_id"      => USER_ID,        // REQUIRED Stored user identifier
     *     "expires"      => EXPIRES,        // REQUIRED Stored expiration in unix timestamp
     *     "redirect_uri" => REDIRECT_URI,   // REQUIRED Stored redirect URI
     *     "scope"        => SCOPE,          // OPTIONAL Stored scope values in space-separated string
     * );
     * @endcode
     *
     * @see http://tools.ietf.org/html/rfc6749#section-4.1
     *
     * @ingroup oauth2_section_4
     */
    public function getAuthorizationCode($code)
    {
        $authorizationCode = $this->em
                ->getRepository('Uacl\Entity\AuthorizationCode')
                ->find($code);

        if ($authorizationCode) {
            return array(
                'authorization_code' => $authorizationCode->getId(),
                'client_id' => $authorizationCode->getClient()->getClientKey(),
                'user_id' => ($authorizationCode->getUser()) ? $authorizationCode->getUser()->getEmail() : null,
                'expires' => $authorizationCode->getExpires()->getTimeStamp(),
                'redirect_uri' => $authorizationCode->getRedirectUri(),
                'scope' => ''
            );
        }

        return false;
    }

    /**
     * Take the provided authorization code values and store them somewhere.
     *
     * This function should be the storage counterpart to getAuthCode().
     *
     * If storage fails for some reason, we're not currently checking for
     * any sort of success/failure, so you should bail out of the script
     * and provide a descriptive fail message.
     *
     * Required for OAuth2::GRANT_TYPE_AUTH_CODE.
     *
     * @param $code
     * Authorization code to be stored.
     * @param $client_id
     * Client identifier to be stored.
     * @param $user_id
     * User identifier to be stored.
     * @param string $redirect_uri
     * Redirect URI(s) to be stored in a space-separated string.
     * @param int $expires
     * Expiration to be stored as a Unix timestamp.
     * @param string $scope
     * (optional) Scopes to be stored in space-separated string.
     *
     * @ingroup oauth2_section_4
     */
    public function setAuthorizationCode($code, $client_id, $user_id, $redirect_uri, $expires, $scope = null)
    {
        $client = $this->em->getRepository('\Uacl\Entity\Client')->findOneBy(array('clientKey' => $client_id));

        $authorizationCode = new \Uacl\Entity\AuthorizationCode;
        $authorizationCode->setId($code)
                ->setClient($client)
                ->setUser($user_id)
                ->setRedirectUri('http://epal.com')
                ->setExpires(new \Datetime(date("Y-m-d H:i:s", $expires)))
        ;
        $this->em->persist($authorizationCode);
        $this->em->flush();

        return array(
            'authorization_code' => $authorizationCode->getId(),
            'client_id' => $authorizationCode->getClient()->getClientKey(),
            'user_id' => ($authorizationCode->getUser()) ? $authorizationCode->getUser()->getEmail() : null,
            'expires' => $authorizationCode->getExpires()->getTimeStamp(),
            'redirect_uri' => $authorizationCode->getRedirectUri(),
            'scope' => ''
        );
    }

    /**
     * once an Authorization Code is used, it must be exipired
     *
     * @see http://tools.ietf.org/html/rfc6749#section-4.1.2
     *
     *    The client MUST NOT use the authorization code
     *    more than once.  If an authorization code is used more than
     *    once, the authorization server MUST deny the request and SHOULD
     *    revoke (when possible) all tokens previously issued based on
     *    that authorization code
     *
     */
    public function expireAuthorizationCode($code)
    {
        $authorizationCode = $this->em
                ->getRepository('Uacl\Entity\AuthorizationCode')
                ->find($code);

        $data = array(
            'authorization_code' => $authorizationCode->getId(),
            'client_id' => $authorizationCode->getClient()->getClientKey(),
            'user_id' => ($authorizationCode->getUser()) ? $authorizationCode->getUser()->getEmail() : null,
            'expires' => $authorizationCode->getExpires()->getTimeStamp(),
            'redirect_uri' => $authorizationCode->getRedirectUri(),
            'scope' => ''
        );

        $this->em->remove($authorizationCode);
        $this->em->flush();

        return $data;
    }

}
