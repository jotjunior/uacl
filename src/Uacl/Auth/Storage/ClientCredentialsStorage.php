<?php

namespace Uacl\Auth\Storage;

use OAuth2\Storage\ClientCredentialsInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Doctrine\ORM\EntityManager;

class ClientCredentialsStorage implements ClientCredentialsInterface, FactoryInterface
{

    /**
     * @var Doctrine\ORM\EntityManager 
     */
    protected $em;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        return $this;
    }

    /**
     * Make sure that the client credentials is valid.
     *
     * @param $client_id
     * Client identifier to be check with.
     * @param $client_secret
     * (optional) If a secret is required, check that they've given the right one.
     *
     * @return
     * TRUE if the client credentials are valid, and MUST return FALSE if it isn't.
     * @endcode
     *
     * @see http://tools.ietf.org/html/rfc6749#section-3.1
     *
     * @ingroup oauth2_section_3
     */
    public function checkClientCredentials($client_id, $client_secret = null)
    {
        $client = $this->em
                ->getRepository('Uacl\Entity\Client')
                ->findOneBy(array('clientKey' => $client_id));

        if ($client) {
            return $client->getClientSecret() == $client_secret;
        }

        return false;
    }

    public function getClientDetails($client_id)
    {

        $client = $this->em
                ->getRepository('Uacl\Entity\Client')
                ->findOneBy(array('clientKey' => $client_id));

        return array(
            'client_id' => $client->getClientKey(),
            'redirect_uri' => $client->getRedirectUri(),
            'grant_types' => $client->getGrantTypes(),
            'secret' => $client->getClientSecret()
        );
    }

    /**
     * Check restricted grant types of corresponding client identifier.
     *
     * If you want to restrict clients to certain grant types, override this
     * function.
     *
     * @param $client_id
     * Client identifier to be check with.
     * @param $grant_type
     * Grant type to be check with
     *
     * @return
     * TRUE if the grant type is supported by this client identifier, and
     * FALSE if it isn't.
     *
     * @ingroup oauth2_section_4
     */
    public function checkRestrictedGrantType($client_id, $grant_type = null)
    {
        $details = $this->getClientDetails($client_id);

        if (isset($details['grant_types']) && $details['grant_types']) {
            $grant_types = explode(' ', $details['grant_types']);
            return in_array($grant_type, (array) $grant_types);
        }

        return true;
    }

    public function isPublicClient($client_id)
    {
        return false;
    }

    public function getClientScope($client_id)
    {
        return false;
    }

}
