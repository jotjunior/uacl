<?php

namespace Uacl\Auth\Storage;

use OAuth2\Storage\RefreshTokenInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

class RefreshTokenStorage implements RefreshTokenInterface, FactoryInterface
{

    /**
     * @var Doctrine\ORM\EntityManager 
     */
    protected $em;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        return $this;
    }

    /**
     * Grant refresh access tokens.
     *
     * Retrieve the stored data for the given refresh token.
     *
     * Required for OAuth2::GRANT_TYPE_REFRESH_TOKEN.
     *
     * @param $refresh_token
     * Refresh token to be check with.
     *
     * @return
     * An associative array as below, and NULL if the refresh_token is
     * invalid:
     * - refresh_token: Stored refresh token identifier.
     * - client_id: Stored client identifier.
     * - expires: Stored expiration unix timestamp.
     * - scope: (optional) Stored scope values in space-separated string.
     *
     * @see http://tools.ietf.org/html/rfc6749#section-6
     *
     * @ingroup oauth2_section_6
     */
    public function getRefreshToken($refresh_token)
    {
        $refreshToken = $this->em->getRepository('Uacl\Entity\RefreshToken')->find($refresh_token);

        if ($refreshToken) {
            return array(
                'refresh_token' => $refreshToken->getId(),
                'client_id' => $refreshToken->getClient()->getClientKey(),
                'user_id' => ($refreshToken->getUser()) ? $refreshToken->getUser()->getUsername() : null,
                'expires' => $refreshToken->getExpires()->getTimestamp(),
                'scope' => '',
                'user' => $refreshToken->getUser()
            );
        }

        return false;
    }

    /**
     * Take the provided refresh token values and store them somewhere.
     *
     * This function should be the storage counterpart to getRefreshToken().
     *
     * If storage fails for some reason, we're not currently checking for
     * any sort of success/failure, so you should bail out of the script
     * and provide a descriptive fail message.
     *
     * Required for OAuth2::GRANT_TYPE_REFRESH_TOKEN.
     *
     * @param $refresh_token
     * Refresh token to be stored.
     * @param $client_id
     * Client identifier to be stored.
     * @param $expires
     * expires to be stored.
     * @param $scope
     * (optional) Scopes to be stored in space-separated string.
     *
     * @ingroup oauth2_section_6
     */
    public function setRefreshToken($refresh_token, $client_id, $user_id, $expires, $scope = null)
    {
        $refreshToken = new \Uacl\Entity\RefreshToken;
        $refreshToken->setId($refresh_token)
                ->setClient($this->em->getRepository('Uacl\Entity\Client')->findOneBy(array('clientKey' => $client_id)))
                ->setUser($this->em->getRepository('Uacl\Entity\User')->findOneBy(array('username' => $user_id)))
                ->setExpires(new \Datetime(date("Y-m-d H:i:s", $expires)));

        $this->em->persist($refreshToken);
        $this->em->flush();

        return array(
            'refresh_token' => $refreshToken->getId(),
            'client_id' => $refreshToken->getClient()->getClientKey(),
            'user_id' => ($refreshToken->getUser()) ? $refreshToken->getUser()->getUsername() : null,
            'expires' => $refreshToken->getExpires()->getTimestamp(),
            'scope' => '',
            'user' => $refreshToken->getUser()
        );
    }

    /**
     * Expire a used refresh token.
     *
     * This is not explicitly required in the spec, but is almost implied.
     * After granting a new refresh token, the old one is no longer useful and
     * so should be forcibly expired in the data store so it can't be used again.
     *
     * If storage fails for some reason, we're not currently checking for
     * any sort of success/failure, so you should bail out of the script
     * and provide a descriptive fail message.
     *
     * @param $refresh_token
     * Refresh token to be expirse.
     *
     * @ingroup oauth2_section_6
     */
    public function unsetRefreshToken($refresh_token)
    {
        $refreshToken = $this->em->getRepository('Uacl\Entity\RefreshToken')->find($refresh_token);

        $data = array(
            'refresh_token' => $refreshToken->getId(),
            'client_id' => $refreshToken->getClient()->getClientKey(),
            'user_id' => ($refreshToken->getUser()) ? $refreshToken->getUser()->getUsername() : null,
            'expires' => $refreshToken->getExpires()->getTimestamp(),
            'scope' => '',
            'user' => $refreshToken->getUser()
        );

        $this->em->remove($refreshToken);
        $this->em->flush();

        return $data;
    }

}
