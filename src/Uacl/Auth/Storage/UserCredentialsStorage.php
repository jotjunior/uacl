<?php

namespace Uacl\Auth\Storage;

use OAuth2\Storage\UserCredentialsInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserCredentialsStorage implements UserCredentialsInterface, FactoryInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        return $this;
    }

    /**
     * Grant access tokens for basic user credentials.
     *
     * Check the supplied username and password for validity.
     *
     * You can also use the $client_id param to do any checks required based
     * on a client, if you need that.
     *
     * Required for OAuth2::GRANT_TYPE_USER_CREDENTIALS.
     *
     * @param $username
     * Username to be check with.
     * @param $password
     * Password to be check with.
     *
     * @return bool
     * TRUE if the username and password are valid, and FALSE if it isn't.
     * Moreover, if the username and password are valid, and you want to
     *
     * @see http://tools.ietf.org/html/rfc6749#section-4.3
     *
     * @ingroup oauth2_section_4
     */
    public function checkUserCredentials($username, $password)
    {
        // tenta o login pelo username
        $user = $this->em->getRepository('Uacl\Entity\User')->findOneBy(array('username' => $username));

        // tenta o login pelo e-mail
        if (!$user) {
            $user = $this->em->getRepository('Uacl\Entity\User')->findOneBy(array('email' => $username));
        }

        // se o usuario existir, valida a senha
        if ($user) {

            if ($user->getPassword() == md5($password)) {
                return true;
            }

            return $user->getPassword() == $user->encryptPassword($password);
        }
        return false;
    }

    /**
     * @return array
     * ARRAY the associated "user_id" and optional "scope" values
     * This function MUST return FALSE if the requested user does not exist or is
     * invalid. "scope" is a space-separated list of restricted scopes.
     * @code
     * return array(
     *     "user_id"  => USER_ID,    // REQUIRED user_id to be stored with the authorization code or access token
     *     "scope"    => SCOPE       // OPTIONAL space-separated list of restricted scopes
     * );
     * @endcode
     */
    public function getUserDetails($username)
    {

        // tenta o login pelo username
        $user = $this->em->getRepository('Uacl\Entity\User')->findOneBy(array('username' => $username));

        // tenta o login pelo e-mail
        if (!$user) {
            $user = $this->em->getRepository('Uacl\Entity\User')->findOneBy(array('email' => $username));
        }

        return array(
            'user_id' => $user->getUsername(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'grant_types' => $user->getGrantTypes(),
            'user_data' => $user->toArray(),
            'scope' => ''
        );
    }

}
