<?php

namespace Uacl\Auth\Permission;

use Zend\Permissions\Acl\Acl as ClassAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\MemcachedCache;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

class Acl extends ClassAcl implements FactoryInterface
{

    /**
     * Papel default (e mínimo) do sistema. Usado para quando não existir 
     * nenhum usuário logado
     */
    const DEFAULT_ROLE = 'guest';

    /**
     * @var Zend\Cache\Storage\StorageInterface
     */
    private $cache;

    /**
     * @var Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Lista com todos os papéis
     * @var array 
     */
    protected $rolesList;

    /**
     * Lista com todos os recursos
     * @var array
     */
    protected $resourcesList;

    /**
     * Lista com todos os privilégios
     * @var array
     */
    protected $privilegesList;

    /**
     * Arquivo de configuração da aplicação
     * @var array  
     */
    protected $config;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $this->cache = $serviceLocator->get('Cache');
        $this->config = $serviceLocator->get('config');
        $this->prepare();
        return $this;
    }

    protected function prepare()
    {
        
        // chaves de identificação do cache para evitar problemas de cache 
        // das permissoes em ambientes compartilhados
        $rolesKey = md5($this->config['application']['name']) . 'uacl.roles.list';
        $resourcesKey = md5($this->config['application']['name']) . 'uacl.resources.list';
        $privilegesKey = md5($this->config['application']['name']) . 'uacl.privileges.list';

        if ($this->cache) {
            if (!($this->rolesList = $this->cache->getItem($rolesKey))) {
                $this->rolesList = $this->em->getRepository('Uacl\Entity\Role')->getList();
                $this->cache->addItem($rolesKey, $this->rolesList);
            }
            if (!($this->resourcesList = $this->cache->getItem($resourcesKey))) {
                $this->resourcesList = $this->em->getRepository('Uacl\Entity\Resource')->getList();
                $this->cache->addItem($resourcesKey, $this->resourcesList);
            }
            if (!($this->privilegesList = $this->cache->getItem($privilegesKey))) {
                $this->privilegesList = $this->em->getRepository('Uacl\Entity\Privilege')->getList();
                $this->cache->addItem($privilegesKey, $this->privilegesList);
            }
        } else {
            $this->rolesList = $this->em->getRepository('Uacl\Entity\Role')->getList();
            $this->resourcesList = $this->em->getRepository('Uacl\Entity\Resource')->getList();
            $this->privilegesList = $this->em->getRepository('Uacl\Entity\Privilege')->getList();
        }

        $this->loadRoles();
        $this->loadResources();
        $this->loadPrivileges();
    }

    private function loadRoles()
    {
        foreach ($this->rolesList as $role) {
            $this->addRole(new Role($role['name']), ($role['parent']) ? new Role($role['parent']) : null);
            if ($role['admin']) {
                $this->allow($role['name'], array(), array());
            }
        }
    }

    private function loadResources()
    {
        foreach ($this->resourcesList as $resource) {
            $this->addResource($resource);
        }
    }

    private function loadPrivileges()
    {
        foreach ($this->privilegesList as $privilege) {
            $this->allow($privilege['role'], $privilege['resource'], $privilege['action']);
        }
    }

}
