<?php

namespace Uacl\Service;

class Client extends AbstractBaseService
{

    protected $entity = 'Uacl\Entity\Client';

    /**
     * Reescreve para criar automaticamente os valores de client_id e client_secret, 
     * além de atribuir o valor de grant_types
     * @param array $data
     * @return Uacl\Entity\Client
     */
    public function insert($data)
    {

        $client_id = md5(uniqid());
        $client_secret = substr(base64_encode(md5($client_id)), 0, 32);

        $data['client_id'] = $client_id;
        $data['client_secret'] = $client_secret;
        $data['grant_types'] = 'password refresh_token client_credentials';

        return parent::insert($data);
    }

}
