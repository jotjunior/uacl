<?php

namespace Uacl\Service;

class User extends AbstractBaseService
{

    protected $entity = 'Uacl\Entity\User';

    /**
     * Reescrevendo o método de remoção do usuário para impedir que o registro
     * seja efetivamente apagado do sistema, fazendo somente uma remoção lógica
     * 
     * @param array $data
     * @return int|array
     * @throws \InvalidArgumentException
     */
    public function delete($data)
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException("Você deve informar um array como parâmetro");
        }
        $entity = $this->getEm()->getReference($this->entity, $data['id']);
        $entity->setActive(0);
        $this->getEm()->persist($entity);
        $this->getEm()->flush();
        return $data['id'];
    }

}
