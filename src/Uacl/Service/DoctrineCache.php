<?php

namespace Uacl\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Doctrine\Common\Cache\MemcachedCache;

/**
 * Classe responsáel por instanciar o MemCached
 */
class DoctrineCache implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');

        $memcached = new \Memcached;
        $memcached->addServer(
                $config['application']['cache']['adapter']['options']['servers'][0][0], $config['application']['cache']['adapter']['options']['servers'][0][1]
        );

        $cache = new MemcachedCache;
        $cache->setMemcached($memcached);

        return $cache;
    }

}
