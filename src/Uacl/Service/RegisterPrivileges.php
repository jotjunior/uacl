<?php

namespace Uacl\Service;

use Doctrine\ORM\EntityManager;
use Zend\Filter\Word\CamelCaseToDash;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

class RegisterPrivileges implements FactoryInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager 
     */
    protected $em;

    /**
     *
     * @var Zend\Cache\Storage\StorageInterface
     */
    protected $cache;

    /**
     * @var array 
     */
    protected $restfulDefaultMethods = array('get', 'getList', 'create', 'update', 'delete');

    /**
     *
     * @var array 
     */
    protected $modules;

    /**
     * Métodos que serão ignorados durante a importação dos recursos
     * @var array 
     */
    protected $unnecessaryMethods = array(
        'notFoundAction',
        'getMethodFromAction'
    );

    /**
     *
     * @var array Dados da configuração da aplicação 
     */
    protected $config;

    /**
     * Construtora da classe que espera receber o EntityManager e a lista de módulos registrados
     * @param \Doctrine\ORM\EntityManager $em
     * @param array $modules
     */
    public function __construct($em = null, $modules = null)
    {
        if ($em && $em instanceof EntityManager) {
            $this->em = $em;
        }
        if ($modules) {
            $this->modules = $modules;
        }
    }

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // montando a lista de modulos
        $moduleManager = $serviceLocator->get('ModuleManager');
        $moduleManager->loadModules();
        $this->modules = $moduleManager->getModules();
        $this->em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $this->cache = $serviceLocator->get('Cache');
        $this->config = $serviceLocator->get('config');

        return $this;
    }

    /**
     * A intenção deste método é varrer todos os módulos em busca de controladores para registrar seus métodos na tabela de Resources.
     * Inicialmente, esses métodos serão automaticamente vinculados a um perfil ADMIN para evitar acesso desnecessário por usuários não autorizados.
     * 
     * @return array 
     * @throws \Exception
     */
    public function registerResources()
    {
        $privileges = array();

        // entidade ROLE com permissão ADMIN
        $role = $this->getEm()->getRepository('Uacl\Entity\Role')->findOneBy(array('admin' => true));
        if (!$role) {
            throw new \Exception('Não existe nenhum papel com privilégio ADMIN');
        }

        foreach ($this->listControllers() as $controller) {

            // recebendo o resource
            $resource = $this->setResourceFromController($controller['name']);

            $privileges[] = $this->setPrivilege($role, $resource, $controller['methods']);
        }

        // limpa o cache para que os novos recursos sejam carregados
        $this->clearCache();

        return $privileges;
    }

    /**
     * Lista todos os módulos ativos na aplicação
     * @return array
     */
    protected function listControllers()
    {
        $pathDir = getcwd() . "/";

        $methods = array();

        $result = array();

        // monta a lista de todos os controladores do modulo UACL
        $controllers = array();
        $moduleConfig = include $pathDir . 'vendor/jotjunior/uacl/config/module.config.php';
        $controllers = array_merge($moduleConfig['controllers']['invokables'], $controllers);

        // monta a lista de todos os controladores dos módulos
        foreach ($this->modules as $module) {
            if (
                    \is_file($pathDir . 'module/' . \ucfirst($module) . '/config/module.config.php') &&
                    false !== ($moduleConfig = include $pathDir . 'module/' . \ucfirst($module) . '/config/module.config.php') &&
                    isset($moduleConfig['controllers']) &&
                    isset($moduleConfig['controllers']['invokables'])
            ) {
                $controllers = array_merge($moduleConfig['controllers']['invokables'], $controllers);
            }
        }

        // monta a lista completa com controladores e seus métodos
        foreach ($controllers as $alias => $fqdnController) {
            if ((new $fqdnController) instanceOf \Zend\Mvc\Controller\AbstractRestfulController) {
                $methods = $this->restfulDefaultMethods;
            }

            $methods = array_merge(
                    $this->filterMethods(get_class_methods($fqdnController)), $methods
            );

            $result[] = array(
                'name' => $alias,
                'methods' => $methods
            );
        }

        return $result;
    }

    /**
     * Filtra os métodos para que sejam retornada somente as actions
     * @param type $methods
     * @return type
     */
    protected function filterMethods($methods)
    {
        $result = array();

        // filtro que converte CamelCase em texto separado por hífen
        $ccFilter = new CamelCaseToDash;

        foreach ($methods as $method) {
            if (substr($method, strlen($method) - 6) == 'Action' && !in_array($method, $this->unnecessaryMethods)) {
                // convertendo CamelCase para traços
                $method = str_replace('Action', '', $method);

                // finalmente o nome da Action preparada para ser cadastrada no Privilégio
                $result[] = strtolower($ccFilter->filter($method));
            }
        }

        return $result;
    }

    /**
     * Verifica se um recurso existe no banco de dados. Se não existir, o cadastra
     * @param string $controller
     * @return \Uacl\Entity\Resource
     */
    protected function setResourceFromController($controller)
    {
        // se não for, ele é gravado no banco de dados
        if (!($resource = $this->getEm()->getRepository('Uacl\Entity\Resource')->findOneBy(array('name' => $controller)))) {
            // preparando os dados para registro do controlador como resource
            $arrController = explode("\\", $controller);
            $moduleName = strtolower($arrController[0]);
            $resourceShortName = strtolower(array_pop($arrController));

            // gravando o controlador como recurso no banco de dados
            $resource = new \Uacl\Entity\Resource;
            $resource->setName($controller)
                    ->setShortName($resourceShortName)
                    ->setModule($moduleName);

            $this->getEm()->persist($resource);
            $this->getEm()->flush();
        }

        return $resource;
    }

    /**
     * @param \Uacl\Entity\Role $role
     * @param \Uacl\Entity\Resoruce $resource
     * @param string $action
     * @return array
     */
    protected function setPrivilege(\Uacl\Entity\Role $role, \Uacl\Entity\Resource $resource, $action)
    {
        $privileges = array();

        if (!is_array($action)) {
            $action[] = $action;
        }

        foreach ($action as $value) {
            // verfiica se o privilégio já existe
            if (!($privilege = $this->getEm()->getRepository('Uacl\Entity\Privilege')->findOneBy(array(
                'resource' => $resource,
                'action' => $value
                    )))) {

                $privilege = new \Uacl\Entity\Privilege;
                $privilege->setResource($resource)
                        ->setRole($role)
                        ->setAction($value);
                $this->getEm()->persist($privilege);
                $this->getEm()->flush();
            }

            $privileges[] = $privilege->toArray();
        }
        return $privileges;
    }

    protected function getEm()
    {
        return $this->em;
    }

    protected function clearCache()
    {
        $rolesKey = md5($this->config['application']['name']) . 'uacl.roles.list';
        $resourcesKey = md5($this->config['application']['name']) . 'uacl.resources.list';
        $privilegesKey = md5($this->config['application']['name']) . 'uacl.privileges.list';

        $this->cache->removeItems(array(
            $rolesKey,
            $resourcesKey,
            $privilegesKey,
        ));
    }

}
