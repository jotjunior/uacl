<?php

namespace Uacl\Service;

use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

abstract class AbstractBaseService implements FactoryInterface
{

    /**
     * Entity Manager
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Hydrator do doctrine
     * @var \DoctrineModule\Stdlib\Hydrator\DoctrineObject
     */
    protected $hydrator;

    /**
     * Nome da entidade que será processada
     * @var string
     */
    protected $entity;

    /**
     * Serviço de log
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * Iniciando manualmente
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct($em = null, $hydrator = null)
    {
        if ($em && $em instanceof EntityManager) {
            $this->em = $em;
        }

        if ($hydrator && $hydrator instanceof DoctrineObject) {
            $this->hydrator = $hydrator;
        }
    }

    /**
     * Iniciando como um serviço através do Module.config
     * @param \Uacl\Service\ServiceLocatorInterface $serviceLocator
     * @return \Uacl\Service\BaseService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $this->em = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $this->hydrator = $serviceLocator->get('Hydrator');
        return $this;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEm()
    {
        return $this->em;
    }

    /**
     * Retorna uma coleção paginada de resultados
     * @param array $options
     * @return \Doctrine\ORM\Query
     */
    public function query($options = [])
    {
        $dql = $this->getEm()
                ->getRepository($this->entity)
                ->createQueryBuilder('e')
                ->select('e');
        if (isset($options['criteria']) && is_array($options['criteria'])) {
            foreach ($options['criteria'] as $option) {
                switch ($option['condition']) {
                    case 'and' :
                        $dql->andWhere($option['statement']);
                        if (isset($option['param'])) {
                            $dql->setParameter($option['param'], $option['value']);
                        }
                        break;
                    case 'or' :
                        $dql->orWhere($option['statement']);
                        if (isset($option['param'])) {
                            $dql->setParameter($option['param'], $option['value']);
                        }
                        break;
                }
            }
        }
        if (isset($options['orderby'])) {
            $dql->orderBy($options['orderby'], (isset($options['order']) ? $options['order'] : null));
        }


        return $dql->getQuery();
    }

    /**
     * Insere um registro no banco de dados
     * @param array $data
     * @return Entity
     * @throws \Uacl\Service\Exception
     */
    public function insert($data)
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException("Você deve informar um array como parâmetro");
        }
        $entity = $this->hydrator->hydrate($data, new $this->entity);
        $this->getEm()->persist($entity);
        $this->getEm()->flush();
        return $entity;
    }

    /**
     * Altera um registro no banco de dados
     * @param array $data
     * @return Entity
     * @throws \Uacl\Service\Exception
     */
    public function update($data)
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException("Você deve informar um array como parâmetro");
        }

        $entity = $this->getEm()
                ->getRepository($this->entity)
                ->find($data['id']);

        $entity = $this->hydrator->hydrate($data, $entity);
        $this->getEm()->persist($entity);
        $this->getEm()->flush();
        return $entity;
    }

    /**
     *
     * @param type $data
     * @return type
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function delete($data)
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException("Você deve informar um array como parâmetro");
        }
        try {

            // converte um unico ID em um array de id para contemplar removao multipla
            if (!is_array($data['id'])) {
                $data['id'] = [$data['id']];
            }

            foreach ($data['id'] as $id) {
                $entity = $this->getEm()->getReference($this->entity, $id);
                $this->getEm()->remove($entity);
            }

            $this->getEm()->flush();
            return $data['id'];
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
