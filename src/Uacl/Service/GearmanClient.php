<?php

namespace Uacl\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

/**
 * Classe responsáel por instanciar o MemCached
 */
class GearmanClient implements FactoryInterface
{

    /**
     *
     * @var \GearmanClient
     */
    protected $client;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        
        $this->client = new \GearmanClient();
        $this->client->addServer($config['gearman']['host'], $config['gearman']['port']);
        return $this->client;
    }

}
