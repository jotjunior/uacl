<?php

namespace Uacl\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

/**
 * Classe responsáel por instanciar o MemCached
 */
class GearmanWorker implements FactoryInterface
{

    /**
     *
     * @var \GearmanWorker
     */
    protected $worker;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        
        $this->worker = new \GearmanWorker();
        $this->worker->addServer($config['gearman']['host'], $config['gearman']['port']);
        return $this->worker;
    }

}
