<?php

namespace Uacl\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\Cache\StorageFactory;

/**
 * Classe responsáel por instanciar o MemCached
 */
class Cache implements FactoryInterface
{

    /**
     * @var Zend\Cache\Storage\StorageInterface; 
     */
    protected $cache;
    
    /**
     * @var array Arquivo de configuração da aplicação
     */
    protected $config;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $this->config = $serviceLocator->get('config');

        $cacheFactory = $this->config['application']['cache'];

        try {
            $this->cache = StorageFactory::factory($cacheFactory);
            return $this->cache;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Limpa o cache do ACL para que os dados sejam atualizados
     */
    public function clearAclCache()
    {
        $this->cache->removeItems(array(
            md5($this->config['application']['name']) . 'uacl.roles.list',
            md5($this->config['application']['name']) . 'uacl.resources.list',
            md5($this->config['application']['name']) . 'uacl.privileges.list',
        ));
    }

    public function clearUaclCache()
    {
        return $this->cache->clearByPrefix('uacl_');
    }

}
