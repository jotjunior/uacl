<?php

namespace Uacl\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

/**
 * Retorna o Hydrator do Doctrine
 */
class Hydrator implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        return new DoctrineObject($entityManager);
    }

}
