<?php

namespace Uacl\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{

    public function invalidTokenAction()
    {

        $this->getResponse()->setStatusCode(403);

        return new JsonModel(array(
            'success' => 0,
            'error' => 'Seu token expirou ou está incorreto.',
            'errorCode' => 'INVALID_TOKEN'
        ));
    }

    public function invalidResourceAction()
    {

        $this->getResponse()->setStatusCode(403);

        return new JsonModel(array(
            'success' => 0,
            'error' => 'O recurso que você deseja acessar não está registrado.',
            'errorCode' => 'INVALID_RESOURCE'
        ));
    }

    public function forbiddenAction()
    {

//        $this->getResponse()->setStatusCode(403);

        return new JsonModel(array(
            'success' => 0,
            'error' => 'Você não tem permissão para acessar esse recurso.',
            'errorCode' => 'FORBIDDEN'
        ));
    }

    /**
     * Cria um usuário válido para acesso ao sistema
     */
    public function createAdminAction()
    {
        $config = $this->getServiceLocator()->get('config');

        if (!key_exists('application', $config)) {
            return new JsonModel(array('success' => 0, 'error' => 'Você deve copiar o arquivo application.global.php no diretório dist e colocá-lo no config/autload'));
        }

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        // cria o CLIENT que fará acesso ao serviço de autenticação
        $client = $em->getRepository('Uacl\Entity\Client')->find(1);
        if (!$client) {
            $client_id = md5(uniqid());
            $client_secret = substr(base64_encode(md5($client_id)), 0, 32);
            $client = $this->getServiceLocator()->get('Uacl\Service\Client')->insert(array(
                'clientKey' => $client_id,
                'clientSecret' => $client_secret,
                'redirectUri' => 'http://task.jot.com.br',
                'grantTypes' => 'password refresh_token client_credentials'
            ));
        }

        // verifica se existe uma ROLE chamada Guest, caso negativo a cria
        $guest = $em->getRepository('Uacl\Entity\Role')->findOneBy(array('name' => 'guest'));
        if (!$guest) {
            $guest = $this->getServiceLocator()->get('Uacl\Service\Role')->insert(array(
                'name' => 'guest',
                'admin' => 0,
                'label' => 'Visitante'
            ));
        }

        // verifica se existe uma ROLE chamada Admin, caso negativo, a cria
        $admin = $em->getRepository('Uacl\Entity\Role')->findOneBy(array('name' => 'admin'));
        if (!$admin) {
            $admin = $this->getServiceLocator()->get('Uacl\Service\Role')->insert(array(
                'name' => 'admin',
                'admin' => 1,
                'label' => 'Visitante'
            ));
        }

        // verifica se existe a linguagem Portugues, caso negativo, a cria
        $language = $em->getRepository('Uacl\Entity\Language')->findOneBy(array('locale' => 'pt_BR'));
        if (!$language) {
            $language = $this->getServiceLocator()->get('Uacl\Service\Language')->insert(array(
                'name' => 'Português',
                'nativeName' => 'Português',
                'locale' => 'pt_BR'
            ));
        }

        // verifica se existe um usuário com poderes administrativos, caso contrário, o cria
        $user = $em->getRepository('Uacl\Entity\User')->findOneBy(array('role' => $admin));
        if (!$user) {
            $language = $this->getServiceLocator()->get('Uacl\Service\User')->insert(array(
                'role' => $admin->getId(),
                'language' => $language->getId(),
                'firstName' => 'Administrador',
                'phone' => '555 1252',
                'email' => 'admin@example.com',
                'username' => 'admin@example.com',
                'password' => 'UACLsuperUser',
                'active' => 1,
                'grantTypes' => 'password'
            ));

            // chama o próprio serviço por CURL para registrar os privilégios

            $httpClient = new \Zend\Http\Client($config['application']['url'] . '/auth/request');
            $httpClient->setMethod(\Zend\Http\Request::METHOD_POST)
                    ->setParameterPost(array(
                        'username' => 'admin@example.com',
                        'password' => 'UACLsuperUser',
                        'grant_type' => 'password',
                        'client_id' => $client->getClientKey(),
                        'client_secret' => $client->getClientSecret()
            ));
            $response = $httpClient->send();

            // convertendo o resultado em um array 
            $tokenData = (array) json_decode($response->getBody());

            // faz uma nova requisição solicitando os dados do usuário logado
            $httpClient->setUri($config['application']['url'] . '/uacl/reset-privileges')
                    ->setMethod(\Zend\Http\Request::METHOD_GET);
            $response = $httpClient->send();

            return new JsonModel(array('success' => 1, 'user' => array('email' => 'admin@example.com', 'password' => 'UACLsuperUser', 'client_id' => $client->getClientKey(), 'client_secret' => $client->getClientSecret())));
        }

        return new JsonModel(array(
            'success' => 0,
            'error' => 'Já existe um usuário com poderes administrativos.',
            'errorCode' => 'ADMIN_EXISTS'
        ));
    }

}
