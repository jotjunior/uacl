<?php

namespace Uacl\Controller;

class RoleController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\Role';
    protected $service = 'Uacl\Service\Role';
    protected $form = 'Uacl\Form\Role';

}
