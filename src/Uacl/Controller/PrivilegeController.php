<?php

namespace Uacl\Controller;

class PrivilegeController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\Privilege';
    protected $service = 'Uacl\Service\Privilege';
    protected $form = 'Uacl\Form\Privilege';

}
