<?php

namespace Uacl\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;
use Zend\Paginator\Paginator;
use Zend\View\Model\JsonModel;

class AbstractBaseController extends AbstractRestfulController
{
    const UNAUTHORIZED_ACCESS_MESSAGE = 'Acesso negado. Seu usuário não possui credenciais para acessar o recurso %s';
    const INVALID_TOKEN_MESSAGE = 'Acesso negado. Token inválido ou não informado';
    const ITEMS_COUNT_PER_PAGE = 20;

    /**
     *
     * @var int
     */
    protected $itemsCountPerPage = self::ITEMS_COUNT_PER_PAGE;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Entidade que será processada nas actions
     * @var Entity
     */
    protected $entity;

    /**
     * Services que serão executados nos métodos POST e PUT
     * @var BaseService
     */
    protected $service;

    /**
     * Formulário que será utilizado para validações e filtros nos métodos POST e PUT
     * @var \Zend\Form\Form;
     */
    protected $form;

    /**
     * Dados do token do usuario
     * @var array
     */
    protected $token;

    /**
     * se isCached for marcado como TRUE, os dados da lista serão gravados no Memcached
     * @var boolean
     */
    protected $isCached = false;

    /**
     * Se TRUE, verifica se o conteudo solicitado pertence ao usuário logado
     */
    protected $checkOauthCredentials = false;

    /**
     * Critérios de filtro que serão mesclados aos enviados por QueryString
     * @var array
     */
    protected $options = [];

    /**
     * Caso positivo, entrega um formato reconhecido pelo datatables
     * @var bool
     */
    protected $isDatatable = false;

    /**
     * @var bool
     */
    protected $isSingleList = false;

    /**
     * Posição das colunas para ordenação
     * @var array
     */
    protected $columns = [];

    /**
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {

        try {

            // variáveis vindos de buscas por datatables
            $echo = $this->params()->fromQuery('draw', 1);
            $draw = $this->params()->fromQuery('draw', 1);

            // Executa os filtros necessários e entrega uma coleção de objetos
            $paginator = $this->queryFromDatabase();

            // montando array do resultado, convertendo objetos para array
            $result = array();
            foreach ($paginator->getCurrentItems() as $entity) {
                if (method_exists($entity, 'toArray')) {
                    $result[] = $entity->toArray();
                } else {
                    $result[] = (new DoctrineObject($this->getEm()))->extract($entity);
                }
            }

            if ($this->isDatatable) {
                $response = [
                    'iDraw' => (int)$draw,
                    'sEcho' => (int)$echo,
                    'iTotalRecords' => $paginator->getTotalItemCount(),
                    'iTotalDisplayRecords' => $paginator->getTotalItemCount(),
                    'aaData' => $result
                ];

            } else if ($this->isSingleList) {
                $response = $result;
            } else {
                // Retornando os dados em JSON
                $response = [
                    'success' => 1,
                    'list' => $result,
                    'pages' => $paginator->getPages(),
                ];
            }


        } catch (\Exception $e) {
            // Retorna o erro encontrado
            $response = array(
                'success' => 0,
                'error' => $e->getMessage(),
                'errorCode' => 'EXCEPTION'
            );
        }


        return new JsonModel($response);
    }

    /**
     * Busca os dados retornados diretamente no banco de dados
     * @return \Zend\View\Model\JsonModel|\Zend\Paginator\Paginator
     */
    protected function queryFromDatabase()
    {

        // Carregando dados do config
        $config = $this->getServiceLocator()
            ->get('config')['application'];

        // Definindo a pagina atual
        $page = $this->params()->fromQuery('page', 1);

        if ($this->isDatatable) {
            // variáveis vindos de buscas por datatables
            $order = $this->params()->fromQuery('order', 'name');
            $length = $this->params()->fromQuery('length', 20);
            $start = $this->params()->fromQuery('start', 20);

            // definindo ordenação
            if (count($order) && isset($order[0]['column']) && isset($this->columns[$order[0]['column']])) {
                $this->options['orderby'] = $this->columns[$order[0]['column']];
                $this->options['order'] = $order[0]['dir'];
            }

            // definindo numero de registros a serem exibidos
            $this->options['item_count_per_page'] = $length;

            // calculando o numero da pagina
            $page = ceil($start / $length) + 1;

        }

        // Opções de ordem e filtro enviadas por QueryString.
        // Caso tenha sido enviado um JSON, o converte para array
        $options = $this->params()->fromQuery('options', []);
        if (is_string($options) && \Util\StdLib\Util::isJson($options)) {
            $options = json_decode($options, true);
        }
        $options = array_merge($options, $this->options);

        // Montando filtros baseado em buscas diretas na QueryString, fora
        // do contexto de $options. Caso encontrado, vai converter para $options

        // instanciando a entidade para testar os atributos
        $entityTest = new $this->entity;

        // filtro que converte palavras com hifem em camelCase
        $camelCase = new \Zend\Filter\Word\DashToCamelCase();

        // varrendo os campos e testando a existencia do campo
        foreach ($this->params()->fromQuery() as $key => $value) {
            $method = 'get' . $camelCase->filter($key);
            $methodIs = 'is' . $camelCase->filter($key);
            $field = \lcfirst($camelCase->filter($key));

            // se o método existir na entidade e for enviado um valor diferente de vazio
            if (((\method_exists($entityTest, $method) || \method_exists($entityTest, $methodIs)) && $value != '') || \strpos($field, ',')) {

                $arrValue = \explode('-', $value);

                if (\strpos($field, ',')) {
                    // checa se é para buscar em mais de um campo. Esta funcionalidade somente funciona para strings
                    $arrValue = explode(',', $field);

                    $arrStatement = [];
                    foreach ($arrValue as $f) {
                        $arrStatement[] = 'e.' . $f . ' LIKE :q';
                    }
                    $statement = '(' . implode(' OR ', $arrStatement) . ')';

                    $options['criteria'][] = [
                        'condition' => 'and',
                        'statement' => $statement,
                        'param' => 'q',
                        'value' => '%' . $value . '%'
                    ];

                } elseif (\is_numeric($value) || \is_bool($value)) {
                    // checa se o valor enviado é numérico, para aplicar a condição =
                    $options['criteria'][] = [
                        'condition' => 'and',
                        'statement' => 'e.' . $field . ' = :' . $field,
                        'param' => $field,
                        'value' => $value
                    ];
                } elseif (\count($arrValue) && \count($arrValue) >= 3 && \checkdate($arrValue[1], \substr($arrValue[2], 0, 2), $arrValue[0])) {
                    // caso seja uma data, aplica o LIKE
                    $options['criteria'][] = [
                        'condition' => 'and',
                        'statement' => 'e.' . $field . '= :' . $field,
                        'param' => $field,
                        'value' => $value
                    ];
                } else {
                    // caso seja uma string, aplica o LIKE
                    $options['criteria'][] = [
                        'condition' => 'and',
                        'statement' => 'e.' . $field . ' LIKE :' . $field,
                        'param' => $field,
                        'value' => '%' . $value . '%'
                    ];
                }
            }
        }

        // se a opção de checar credenciais estiver ativa, acrescenta a verificação do user_id, caso o campo exista
        if ($this->checkOauthCredentials && \method_exists($entityTest, 'getUser')) {

            // carregando o service oAuth2 para usar o ID do usuário logado
            $oauthServer = $this->getServiceLocator()
                ->get('oauth2provider.server.main');

            $oauthClient = $oauthServer->getAccessTokenData();

            if (!$oauthClient['user']['role']['admin']) {
                $options['criteria'][] = [
                    'condition' => 'and',
                    'statement' => 'e.user = :user',
                    'param' => 'user',
                    'value' => $oauthClient['user']['id']
                ];
            }
        }


        // Checa se o controllador vai buscar os dados do cache
        if ($this->isCached) {

            // Instanciando o cache
            $cache = $this->getServiceLocator()
                ->get('Cache');

            // Gerando chave do cache
            $cacheKey = 'uacl_' . md5(serialize($options) . $this->entity . '-page-' . $page);

            // testa se a chave do cache já foi criada, caso positivo, retorna o resultado em cache
            if ($cache->hasItem($cacheKey)) {
                $response = $cache->getItem($cacheKey);
                return new JsonModel($response);
            }
        }

        // Executa a query com a lista dos resultados
        $query = $this->getServiceLocator()
            ->get($this->service)
            ->query($options);

        // verifica se o projeto permite alterar o numero de itensp or pagina e checa se este valor foi setado
        if ($config['options']['allow_change_items_count_per_page'] && isset($options['items_count_per_page'])) {
            $this->itemsCountPerPage = $options['items_count_per_page'];
        }

        // pagina o resultado
        $paginator = new Paginator(
            new DoctrinePaginator(new ORMPaginator($query))
        );
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage($this->itemsCountPerPage);

        return $paginator;
    }

    /**
     * Retorna uma instância do EntityManager
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEm()
    {
        if (!($this->em instanceOf EntityManager)) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    /**
     * Restful GET - Retorna um registro a partir de seu ID
     * @param mixed $id
     * @return JsonModel
     */
    public function get($id)
    {

        $criteria['id'] = $id;

        // cria entidade de testes para checar existencia de metodos
        $entityTest = new $this->entity;

        // se foi marcada opção de checar credenciais e a entidade possuir getUser, adiciona o filtro
        if ($this->checkOauthCredentials && method_exists($entityTest, 'getUser')) {

            // Busca somente o dado se ele pertencer ao usuário
            $oauthServer = $this->getServiceLocator()
                ->get('oauth2provider.server.main');

            $client = $oauthServer->getAccessTokenData();

            if (!$client['user']['role']['admin']) {
                $criteria['user'] = $client['user']['id'];
            }
        }

        // instancia a entidade a partir do ID enviado
        $entity = $this->getEm()->getRepository($this->entity)->findOneBy($criteria);

        if (!$entity) {
            return new JsonModel([
                'success' => 0,
                'error' => 'Nenhum registro encontrado',
                'errorCode' => 'EMPTY_RECORD'
            ]);
        }

        if (method_exists($entity, 'toArray')) {
            $data = $entity->toArray();
        } else {
            $data = (new DoctrineObject($this->getEm()))->extract($entity);
        }

        return new JsonModel(array('success' => 1, 'data' => $data));
    }

    /**
     * Adiciona um registro no banco de dados
     * @param mixed $data
     * @return JsonModel
     */
    public function create($data)
    {

        try {

            // instancia o formulário para validar os dados enviados
            $form = new $this->form($this->getEm());
            $form->setData($data);

            // se os dados forem válidos, procede com a inserção
            if ($form->isValid()) {

                if (!count($form->getData())) {
                    return new JsonModel(array('success' => 0, 'error' => 'Nenhum dado encontrado no formulário', 'errorCode' => 'EMPTY_FORM'));
                }

                $service = $this->getServiceLocator()->get($this->service);
                $entity = $service->insert($form->getData());

                if (method_exists($entity, 'toArray')) {
                    $data = $entity->toArray();
                } else {
                    $data = (new DoctrineObject($this->getEm()))->extract($entity);
                }
                return new JsonModel(array('success' => 1, 'data' => $data));
            } else {
                // se encontrou mensagens de erro do formulário, retorna a lista de erros
                return new JsonModel(array('success' => 0, 'error' => $form->getMessages(), 'errorCode' => 'FORM_VALIDATION_ERROR'));
            }
        } catch (\Exception $e) {
            return new JsonModel(array('success' => 0, 'error' => $e->getMessage(), 'errorCode' => 'EXCEPTION'));
        }
    }

    /**
     * Altera um registro do banco de dados
     * @param mixed $id
     * @param mixed $data
     * @return JsonModel
     */
    public function update($id, $data)
    {

        try {

            // adicionando o $id ao array $data para atender às necessidades do service
            $data['id'] = $id;

            // instancia o formulário para validar os dados enviados
            $form = new $this->form($this->getEm());
            $form->setData($data);

            // se os dados forem válidos, procede com a inserção
            if ($form->isValid()) {

                if (!count($form->getData())) {
                    return new JsonModel(array('success' => 0, 'error' => 'Nenhum dado encontrado no formulário'));
                }

                $post = $form->getData();

                $postData = [];
                foreach ($post as $key => $value) {
                    if ($value !== null) {
                        $postData[$key] = $value;
                    }
                }

                $service = $this->getServiceLocator()->get($this->service);
                $entity = $service->update($postData);

                // se existir a coluna setUpdated, atualizada com a data e hora atuais
                if (\method_exists($entity, 'setUpdated')) {
                    $entity->setUpdated(new \DateTime('now'));
                }

                // checa se ha o metodo toarray, caso contrario, extrai pelo hydrator
                if (\method_exists($entity, 'toArray')) {
                    $data = $entity->toArray();
                } else {
                    $data = (new DoctrineObject($this->getEm()))->extract($entity);
                }
                return new JsonModel(array('success' => 1, 'data' => $data));
            } else {
                // se encontrou mensagens de erro do formulário, retorna a lista de erros
                return new JsonModel(array('success' => 0, 'error' => $form->getMessages(), 'errorCode' => 'FORM_VALIDATION_ERROR'));
            }
        } catch (\Exception $e) {
            return new JsonModel(array('success' => 0, 'error' => $e->getMessage(), 'errorCode' => 'EXCEPTION'));
        }
    }

    /**
     * Remove um registro do banco de dados
     * @param mixed $id
     * @return JsonModel
     */
    public function delete($id)
    {
        try {

            // identificando se há mais de um ID a ser removido
            if (strpos($id, '-')) {
                $data['id'] = explode('-', $id);
            } else {
                $data['id'] = $id;
            }

            // instancia o serviço que fará a remoção
            $service = $this->getServiceLocator()->get($this->service);

            $result = $service->delete($data);
            return new JsonModel(array('success' => 1, 'id' => $result));
        } catch (\Exception $e) {
            $errorMessage = $e->getMessage();

            if (stripos($errorMessage, 'foreign key constraint fails')) {
                $errorMessage = 'Não foi possível remover este(s) item(ns) pois há '
                    . 'registros vinculados a este(s) em outros cadastros.';
            }

            return new JsonModel(array('success' => 0, 'error' => $errorMessage, 'errorCode' => 'DELETE_ERROR'));
        }
    }

    /**
     * Testa permissões de acesso antes de entregar de dispachar o controlador
     * @param MvcEvent $e
     * @return mixed|void
     */
    public function onDispatch(MvcEvent $e)
    {

        // rota processada
        $routerMatch = $e->getRouteMatch();

        // service locator
        $serviceLocator = $this->getServiceLocator();

        // dados da configuração
        $config = $serviceLocator->get('config');

        // define a ação utilizada
        $actionName = $routerMatch->getParam('action');

        if (!$actionName) {
            switch ($e->getRequest()->getMethod()) {
                case 'GET' :
                    $id = $this->params()->fromRoute('id', 0);
                    if ($id) {
                        $actionName = 'get';
                    } else {
                        $actionName = 'getList';
                    }
                    break;
                case 'POST' :
                    $actionName = 'create';
                    break;
                case 'PUT' :
                    $actionName = 'update';
                    break;
                case 'DELETE' :
                    $actionName = 'delete';
                    break;
            }
        }

        // libera acesso irrestrito para criação do usuário
        if ($actionName == 'create-admin') {
            return;
        }

        // se não encontra a configuração, retorna uma exceção
        if (!isset($config['application'])) {
            throw new \RuntimeException("Não há configuração para a aplicação atual. Favor verificar a existencia do "
                . "arquivo 'config/autoload/application.global.php', encontrado no diretório DIST do módulo UACL ");
        }

        // verifica se o ACL foi desabilitado no arquivo de configuração
        if (isset($config['application']['acl']) && !$config['application']['acl']['enabled']) {
            return;
        }

        // carrega a lista de permissões de acesso e já define o papel default
        $acl = $serviceLocator->get('Permissions');
        $role = $acl::DEFAULT_ROLE;

        // response HTTP que será usado nos redirecionamentos
        $response = $e->getResponse();

        // se o controlador não estiver registrado, redireciona o usuário para um erro
        if (false === $acl->hasResource($routerMatch->getParam('controller'))) {
            $e->getTarget()
                ->redirect()
                ->toRoute('invalid-resource');
            // define o erro como acesso negado
            $response->setStatusCode(403)
                ->sendHeaders();
            $e->stopPropagation();
            exit();
        } else {
            // se o controlador foi encontrado, checa se o token é válido
            $server = $serviceLocator->get('oauth2provider.server.main');
            $isValid = $server->verifyResourceRequest();

            // libera acesso aos recursos informados no arquivo de configura'ão
            if (
                !$isValid &&
                (!in_array($routerMatch->getParam('controller'), $config['application']['acl']['public_controllers'])) &&
                (!in_array($actionName, $config['application']['acl']['public_actions']))
            ) {
                // se os recursos não tiverem sido liberados, leva o usuário à página de token inválido
                return $e->setResult(new JsonModel([
                    'error' => self::INVALID_TOKEN_MESSAGE,
                    'errorCode' => 'INVALID_TOKEN'
                ]));

            } else {
                // por fim, busca as credenciais do usuário e valida sua permissão de acesso
                $client = $server->getAccessTokenData();
                $role = ($client['user']) ? $client['user']['role']['name'] : $role;
                if (
                    !in_array($routerMatch->getParam('controller'), $config['application']['acl']['public_controllers']) &&
                    !in_array($actionName, $config['application']['acl']['public_actions']) &&
                    !$acl->isAllowed($role, $routerMatch->getParam('controller'), $actionName)
                ) {
                    return $e->setResult(new JsonModel([
                        'error' => \sprintf(self::UNAUTHORIZED_ACCESS_MESSAGE, $resource),
                        'errorCode' => 'UNAUTHORIZED_ACCESS'
                    ]));
                }
            }
        }

        return parent::onDispatch($e);

    }

    /**
     * @return JsonModel
     */
    protected function disableMethod()
    {
        return new JsonModel([
            'success' => 0,
            'error' => 'Este método está desabilidado',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

    /**
     * @return array
     */
    protected function getTokenData()
    {

        if (!$this->token) {
            $oAuthServer = $this->getServiceLocator()->get('oauth2provider.server.main');
            $this->token = $oAuthServer->getAccessTokenData();
        }
        return $this->token;

    }

}
