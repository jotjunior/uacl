<?php

namespace Uacl\Controller;

use Zend\View\Model\JsonModel;
use Zend\Validator\Uri as UriValidator;

class ResetPasswordController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\User';

    public function create($data)
    {

        // verifica se o usuário existe
        $user = $this->getEm()
                ->getRepository('Uacl\Entity\User')
                ->findOneBy(array('email' => $data['email']));

        // verifica se a URL de redirecionamento é válida
        $uri = new UriValidator();
        if (!$uri->isValid($data['redirect_uri'])) {
            return new JsonModel(array('success' => 0, 'error' => $uri->getMessages(), 'errorCode' => 'INVALID_URI'));
        }

        if (!$user) {
            return new JsonModel(array('success' => 0, 'error' => 'O e-mail informado não pertence a nenhum usuário.', 'errorCode' => 'INVALID_EMAIL'));
        }

        // cria um novo código de verificação e o atribui ao usuário
        try {
            $codigo = preg_replace("/[^A-Za-z0-9]/", "", base64_encode(rand() . uniqid()));
            $user->setActivationKey($codigo);
            $this->getEm()->persist($user);
            $this->getEm()->flush();
        } catch (\RuntimeException $e) {
            return new JsonModel(array('success' => 0, 'error' => $e->getMessage()));
        }

        // envia um e-mail para o usuário com o link de reset 
        try {
            $this->getServiceLocator()->get('Messenger')
                    ->setTemplate('reset-password')
                    ->setData(array(
                        'message' => '',
                        'user' => $user,
                        'redirectUri' => $data['redirect_uri'],
                    ))
                    ->setSubject('Recuperação de Senha')
                    ->setFrom($this->getEm()->getReference('Uacl\Entity\User', 1))
                    ->addTo($user)
                    ->saveMessage()
                    ->send();

            return new JsonModel(array('success' => 1, 'data' => array('firstName' => $user->getFirstName(), 'email' => $user->getEmail())));
        } catch (\Exception $e) {
            return new JsonModel(array('success' => 0, 'error' => $e->getMessage(), 'errorCode' => 'EXCEPTION'));
        }
    }

}
