<?php

namespace Uacl\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ResetPrivilegesController extends AbstractRestfulController
{

    public function getList()
    {
        // resetando o cache
        $this->getServiceLocator()
                ->get('Cache')
                ->removeItems(array(
                    'uacl.roles.list',
                    'uacl.resources.list',
                    'uacl.privileges.list',
        ));

        // ferificando se há novos privilegios a serem registrados
        $service = $this->getServiceLocator()->get('Uacl\Service\RegisterPrivileges');
        $privileges = $service->registerResources();
        return new JsonModel(array('success' => 1, 'data' => $privileges));
    }

}
