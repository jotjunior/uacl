<?php

namespace Uacl\Controller;

use Zend\View\Model\JsonModel;

class RegisterController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\User';
    protected $service = 'Uacl\Service\User';
    protected $form = 'Uacl\Form\User';

    public function create($data)
    {
        // instancia o formulário para validar os dados enviados
        $form = new $this->form($this->getEm());
        $form->setData($data);

        // se os dados forem válidos, procede com a inserção
        if ($form->isValid()) {

            if (!count($form->getData())) {
                return new JsonModel(array('success' => 0, 'error' => 'Nenhum dado encontrado no formulário', 'errorCode' => 'EMPTY_FORM'));
            }

            // gerando codigo unico para activation key
            $codigo = preg_replace("/[^A-Za-z0-9]/", "", base64_encode(rand() . uniqid()));

            $service = $this->getServiceLocator()->get($this->service);
            $data['activationKey'] = $codigo;

            $entity = $service->insert($data);
            if (method_exists($entity, 'toArray')) {
                $data = $entity->toArray();
            } else {
                $data = (new DoctrineObject($this->getEm()))->extract($entity);
            }

            try {
                // enviando um e-mail de confirmação solicitando ativação da conta
                $data['message'] = '';
                $data['password'] = $this->params()->fromPost('password');
                $mail = $this->getServiceLocator()->get('Messenger')
                        ->setTemplate('activation')
                        ->setData($data)
                        ->setSubject('[ePal] Ativação de conta')
                        ->setFrom($this->getEm()->getReference($this->entity, 1))
                        ->addTo($entity)
                        ->saveMessage()
                        ->send();
            } catch (\RuntimeException $e) {
                // não faz nada, por enquanto
            }

            return new JsonModel(array('success' => 1, 'data' => $data));
        } else {
            // se encontrou mensagens de erro do formulário, retorna a lista de erros
            return new JsonModel(array('success' => 0, 'error' => $form->getMessages(), 'errorCode' => 'EXCEPTION'));
        }
    }

}
