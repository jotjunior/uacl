<?php

namespace Uacl\Controller;

use Zend\View\Model\JsonModel;

class MenuController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\Menu';
    protected $service = 'Uacl\Service\Menu';
    protected $form = 'Uacl\Form\Menu';
    protected $isCached = true;

    public function getList()
    {

        // Tipo de retorno (se árvore ou lista)
        $returnType = $this->params()->fromQuery('type', 'list');

        switch ($returnType) {
            case 'tree':

                // lista todas os itens do menu
                $nodes = $this->getEm()
                        ->getRepository($this->entity)
                        ->findBy(['parent' => null], ['position' => 'asc']);

                $result = [];
                foreach ($nodes as $node) {
                    $result[] = $node->getTree($node);
                }

                return new JsonModel([
                    'success' => 1,
                    'list' => $result
                ]);

            case 'list' :
            default :
                return parent::getList();
        }
    }

    public function update($id, $data)
    {

        if (isset($data['name'])) {
            return parent::update($id, $data);
        } else {
            $hydrator = $this->getServiceLocator()
                    ->get('hydrator');

            // montando a lista de resultados de forma linear
            $result = [];
            foreach ($data as $node) {
                $this->walker($node, $result);
            }

            // alterando os dados de cada objeto
            foreach ($result as $position => $node) {
                $entity = $hydrator->hydrate($node, new \Uacl\Entity\Menu);
                $entity->setPosition($position);

                $this->getEm()->persist($entity);
            }

            $this->getEm()->flush();

            return new JsonModel([
                'success' => 1,
                'list' => $result
            ]);
        }
    }

    public function walker($parent, &$result, $parentId = null)
    {
        $p = $parent['item'];
        $p['parent'] = $parentId;

        $result[] = $p;
        if (isset($parent['children'])) {
            foreach ($parent['children'] as $child) {
                $this->walker($child, $result, $parent['item']['id']);
            }
        }
    }

}
