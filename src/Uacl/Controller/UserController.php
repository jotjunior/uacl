<?php

namespace Uacl\Controller;

use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Validator\ValidatorChain;

class UserController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\User';
    protected $service = 'Uacl\Service\User';
    protected $form = 'Uacl\Form\User';

    public function update($id, $data)
    {
        // adicionando o $id ao array $data para atender às necessidades do service
        $data['id'] = $id;

        // instancia o formulário para validar os dados enviados
        $form = new $this->form($this->getEm());

        $user = $this->getEm()
                ->getRepository($this->entity)
                ->find($id);

        // removendo validadores caso e-mail e username não tenham sido alterados
        if (!isset($data['email']) || $data['email'] == $user->getEmail()) {
            $emailValidatorChain = new ValidatorChain;
            $validators = $form->getInputFilter()
                    ->get('email')
                    ->getValidatorChain()
                    ->getValidators();

            foreach ($validators as $validator) {
                if (!($validator['instance'] instanceOf \DoctrineModule\Validator\NoObjectExists)) {
                    $emailValidatorChain->attach($validator['instance']);
                }
            }

            $form->getInputFilter()->get('email')->setValidatorChain($emailValidatorChain);
        }

        if (!isset($data['username']) || $data['username'] == $user->getUsername()) {
            $usernameValidatorChain = new ValidatorChain;

            $validators = $form->getInputFilter()
                    ->get('username')
                    ->getValidatorChain()
                    ->getValidators();

            foreach ($validators as $validator) {
                if (!($validator['instance'] instanceOf \DoctrineModule\Validator\NoObjectExists)) {
                    $usernameValidatorChain->attach($validator['instance']);
                }
            }

            $form->getInputFilter()->get('username')->setValidatorChain($usernameValidatorChain);
        }

        if (!isset($data['password']) || $data['password'] == '') {
            $passwordValidatorChain = new ValidatorChain;

            $validators = $form->getInputFilter()
                    ->get('password')
                    ->setRequired(false)
                    ->getValidatorChain()
                    ->getValidators();

            foreach ($validators as &$validator) {
                if (!($validator['instance'] instanceOf \Zend\Validator\NotEmpty)) {
                    $passwordValidatorChain->attach($validator['instance']);
                }
            }

            $form->getInputFilter()->get('password')->setValidatorChain($passwordValidatorChain);

            // removendo o campo para que ele não seja alterado
            unset($data['password']);
        }

        // injetando os dados no formulário para validação
        $form->setData($data);

        // se os dados forem válidos, procede com a inserção
        if ($form->isValid()) {

            if (!count($form->getData())) {
                return new JsonModel(array('success' => 0, 'error' => 'Nenhum dado encontrado no formulário', 'errorCode' => 'EMPTY_FORM'));
            }

            $service = $this->getServiceLocator()->get($this->service);
            $entity = $service->update($data);
            if (method_exists($entity, 'toArray')) {
                $data = $entity->toArray();
            } else {
                $data = (new DoctrineObject($this->getEm()))->extract($entity);
            }
            return new JsonModel(array('success' => 1, 'data' => $data));
        } else {
            // se encontrou mensagens de erro do formulário, retorna a lista de erros
            return new JsonModel(array('success' => 0, 'error' => $form->getMessages(), 'errorCode' => 'EXCEPTION'));
        }
    }

    public function documentationAction()
    {
        $documentation = [
            'getList' => [
                'name' => 'Lista de usuários',
                'description' => '',
                'resource' => '/uacl/user?access_token=&lt;access_token&gt;',
                'request' => [
                    'method' => 'GET',
                    'data' => []
                ],
                'response' => [
                    'data' => [
                        'success' => [
                            'type' => 'boolean',
                        ],
                        'list' => [
                            'type' => 'collection',
                            'structure' => [
                                
                            ]
                        ],
                        'data' => [
                            
                        ],
                        'error' => [
                            
                        ],
                        'errorCode' => [
                            
                        ]
                    ],
                    'example' => [
                        'access_token' => '5d082ff3a1fd48bdace77e4edd1ee219cc1735bd',
                        'expires_in' => 3600,
                        'token_type' => 'bearer',
                        'scope' => '',
                        'refresh_token' => '823606f6c584f80fd870d988e43c0d0e28720f41',
                    ]
                ]
            ],
            'get' => [
                'name' => 'Dados do usuário',
                'description' => '',
                'resource' => '/uacl/user/&lt;id&gt;',
                'request' => [
                    'method' => 'GET',
                    'data' => []
                ],
                'response' => [
                    'data' => [
                        'success' => [
                            'type' => 'boolean',
                        ],
                        'list' => [
                        ],
                        'data' => [
                            'type' => 'object',
                            'structure' => [
                                
                            ]
                        ],
                        'error' => [
                            
                        ],
                        'errorCode' => [
                            
                        ]
                    ],
                    'example' => [
                        'access_token' => '5d082ff3a1fd48bdace77e4edd1ee219cc1735bd',
                        'expires_in' => 3600,
                        'token_type' => 'bearer',
                        'scope' => '',
                        'refresh_token' => '823606f6c584f80fd870d988e43c0d0e28720f41',
                    ]
                ]
            ],
            'create' => [
                'name' => 'Novo usuário',
                'description' => '',
                'resource' => '/uacl/user',
                'request' => [
                    'method' => 'POST',
                    'data' => []
                ],
                'response' => [
                    'data' => [
                        'success' => [
                            'type' => 'boolean',
                        ],
                        'list' => [
                            'type' => 'collection',
                            'structure' => [
                            ]
                        ],
                        'data' => [
                            
                        ],
                        'error' => [
                            
                        ],
                        'errorCode' => [
                            
                        ]
                    ],
                    'example' => [
                        'access_token' => '5d082ff3a1fd48bdace77e4edd1ee219cc1735bd',
                        'expires_in' => 3600,
                        'token_type' => 'bearer',
                        'scope' => '',
                        'refresh_token' => '823606f6c584f80fd870d988e43c0d0e28720f41',
                    ]
                ]
            ],
            'update' => [
                'name' => 'Alterar usuário',
                'description' => '',
                'resource' => '/uacl/user/&lt;id&gt;',
                'request' => [
                    'method' => 'PUT',
                    'data' => []
                ],
                'response' => [
                    'data' => [
                        'success' => [
                            'type' => 'boolean',
                        ],
                        'list' => [
                            'type' => 'collection',
                            'structure' => [
                            ]
                        ],
                        'data' => [
                            
                        ],
                        'error' => [
                            
                        ],
                        'errorCode' => [
                            
                        ]
                    ],
                    'example' => [
                        'access_token' => '5d082ff3a1fd48bdace77e4edd1ee219cc1735bd',
                        'expires_in' => 3600,
                        'token_type' => 'bearer',
                        'scope' => '',
                        'refresh_token' => '823606f6c584f80fd870d988e43c0d0e28720f41',
                    ]
                ]
            ],
            'delete' => [
                'name' => 'Remover usuário',
                'description' => '',
                'resource' => '/uacl/user/&lt;id&gt;',
                'request' => [
                    'method' => 'DELETE',
                    'data' => [
                        
                    ]
                ],
                'response' => [
                    'data' => [
                        'success' => [
                            'type' => 'boolean',
                        ],
                        'list' => [
                            'type' => 'collection',
                            'structure' => [
                            ]
                        ],
                        'data' => [
                            
                        ],
                        'error' => [
                            
                        ],
                        'errorCode' => [
                            
                        ]
                    ],
                    'example' => [
                        'access_token' => '5d082ff3a1fd48bdace77e4edd1ee219cc1735bd',
                        'expires_in' => 3600,
                        'token_type' => 'bearer',
                        'scope' => '',
                        'refresh_token' => '823606f6c584f80fd870d988e43c0d0e28720f41',
                    ]
                ]
            ],
            
        ];

        $view = new ViewModel(['documentation' => $documentation]);
        $view->setTemplate('doc');
        return $view;
    }

}
