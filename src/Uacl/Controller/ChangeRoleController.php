<?php

namespace Uacl\Controller;

use Zend\View\Model\JsonModel;

class ChangeRoleController extends AbstractBaseController
{

    public function update($id, $data)
    {

        $privilege = $this->getEm()
                ->getRepository('Uacl\Entity\Privilege')
                ->find($id);

        $privilege->setRole($this->getEm()->getReference('Uacl\Entity\Role', $data['role']));

        $this->getEm()->persist($privilege);
        $this->getEm()->flush();

        return new JsonModel([
            'success' => 1,
            'data' => $privilege->toArray()
        ]);
    }

    public function getList()
    {

        $this->getResponse()->setStatusCode(\Zend\Http\Response::STATUS_CODE_405);
        return new JsonModel([
            'success' => 0,
            'error' => 'Método inválido',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

    public function get($id)
    {

        $this->getResponse()->setStatusCode(\Zend\Http\Response::STATUS_CODE_405);
        return new JsonModel([
            'success' => 0,
            'error' => 'Método inválido',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

    public function create($data)
    {

        $this->getResponse()->setStatusCode(\Zend\Http\Response::STATUS_CODE_405);
        return new JsonModel([
            'success' => 0,
            'error' => 'Método inválido',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

    public function delete($id)
    {

        $this->getResponse()->setStatusCode(\Zend\Http\Response::STATUS_CODE_405);
        return new JsonModel([
            'success' => 0,
            'error' => 'Método inválido',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

}
