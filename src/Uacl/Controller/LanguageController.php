<?php

namespace Uacl\Controller;

class LanguageController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\Language';
    protected $service = 'Uacl\Service\Language';
    protected $form = 'Uacl\Form\Language';
    protected $isCached = false;

}
