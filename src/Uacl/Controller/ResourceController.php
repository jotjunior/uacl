<?php

namespace Uacl\Controller;

use Zend\View\Model\JsonModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

class ResourceController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\Resource';
    protected $service = 'Uacl\Service\Resource';
    protected $form = 'Uacl\Form\Resource';

    /**
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {

        try {
            $list = $this->getEm()
                    ->getRepository($this->entity)
                    ->getPermissionMap();

            // setando a página atual
            $page = $this->params()->fromQuery('page', 1);

            // checando se há opções enviadas por querystring
            $options = $this->params()->fromQuery('options', []);
            if (is_string($options) && \Util\StdLib\Util::isJson($options)) {
                $options = json_decode($options, true);
            }

            if (isset($options['items_count_per_page'])) {
                $this->itemsCountPerPage = $options['items_count_per_page'];
            }

            // instanciando o paginator
            $paginator = new Paginator(new ArrayAdapter($list));
            $paginator->setCurrentPageNumber($page);
            $paginator->setDefaultItemCountPerPage($this->itemsCountPerPage);

            // montando array do resultado
            $result = array();
            foreach ($paginator->getCurrentItems() as $entity) {
                $result[] = $entity;
            }

            return new JsonModel(array(
                'success' => 1,
                'list' => $result,
                'pages' => $paginator->getPages(),
            ));
        } catch (\Exception $e) {
            return new JsonModel(array('success' => 0, 'error' => $e->getMessage(), 'errorCode' => 'EXCEPTION'));
        }
    }

}
