<?php

namespace Uacl\Controller;

use Zend\View\Model\JsonModel;

class ValidateController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\User';

    public function get($id)
    {

        // verifica se o usuário existe
        $user = $this->getEm()
                ->getRepository($this->entity)
                ->findOneBy(array('activationKey' => $id));
        
        
        if(!$user) {
            return new JsonModel([
                'success' => 0,
                'error' => 'Usuário inválido',
                'errorCode' => 'INVALID_USER'
            ]);
        }
        
        $user->setActive(1);
        $this->getEm()->persist($user);
        $this->getEm()->flush();
        
        return new JsonModel([
            'success' => 1,
            'data' => $user->toArray(),
        ]);
        
        
    }

    public function getList()
    {
        return new JsonModel([
            'success' => 0,
            'error' => 'Método desabilitado',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

    public function create($data)
    {
        return new JsonModel([
            'success' => 0,
            'error' => 'Método desabilitado',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

    public function update($id, $data)
    {
        return new JsonModel([
            'success' => 0,
            'error' => 'Método desabilitado',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

    public function delete($id)
    {
        return new JsonModel([
            'success' => 0,
            'error' => 'Método desabilitado',
            'errorCode' => 'INVALID_METHOD'
        ]);
    }

}
