<?php

namespace Uacl\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use OAuth2Provider\Controller\ControllerInterface;

class AuthController extends AbstractRestfulController implements ControllerInterface
{

    public function authorizeAction()
    {
        return new JsonModel(array(
            'success' => 0,
            'error' => 'Erro: O endpoint Authorize não é suportado no método UserCredentials',
            'errorCode' => 'OAUTH_ERROR'
        ));
    }

    public function requestAction()
    {

        $server = $this->getServiceLocator()->get('oauth2provider.server.main');
        $response = $server->handleTokenRequest();
        $params = $response->getParameters();

        return new JsonModel($params);
    }

    public function userAction()
    {
        $server = $this->getServiceLocator()->get('oauth2provider.server.main');
        $isValid = $server->verifyResourceRequest(null);
        if ($isValid) {
            $data = $server->getAccessTokenData();
            $user = $data['user'];
            return new JsonModel($user);
        }

        $params['sucess'] = 0;
        $params['errorCode'] = 'INVALID_TOKEN';
        $params['error'] = 'Token inválido';
        return new JsonModel($params);
    }

    public function resourceAction($scope = null)
    {
        $server = $this->getServiceLocator()->get('oauth2provider.server.main');
        $isValid = $server->verifyResourceRequest($scope);
        $responseParam = $server->getResponse()->getParameters();

        $params = array();
        $params['success'] = $isValid;

        if (!$isValid) {
            $params['error'] = isset($responseParam['error']) ? $responseParam['error'] : "Requisição inválida";
            $params['errorCode'] = 'INVALID_TOKEN';
            $params['message'] = isset($responseParam['error_description']) ? $responseParam['error_description'] : "Seu access token não é válido!";
        } else {
            $params['message'] = "Seu Access Token é válido!";
        }

        return new JsonModel($params);
    }

    public function tokenAction()
    {
        $server = $this->getServiceLocator()->get('oauth2provider.server.main');
        $isValid = $server->verifyResourceRequest(null);
        if ($isValid) {
            $data = $server->getAccessTokenData();
            return new JsonModel($data);
        }

        $params['sucess'] = 0;
        $params['errorCode'] = 'INVALID_TOKEN';
        $params['error'] = 'Token inválido';
        return new JsonModel($params);
    }

    public function documentationAction()
    {
        $documentation = [
            'request' => [
                'name' => 'Autenticação por oauth2',
                'description' => 'Método de autenticação utilizado pela UACL API.',
                'resource' => '/auth/request',
                'request' => [
                    'method' => 'POST',
                    'data' => [
                        'username' => [
                            'type' => 'string',
                            'required' => true,
                        ],
                        'password' => [
                            'type' => 'string',
                            'required' => true,
                        ],
                        'client_id' => [
                            'type' => 'string',
                            'required' => true
                        ],
                        'client_secret' => [
                            'type' => 'string',
                            'required' => true
                        ],
                        'grant_type' => [
                            'type' => 'string',
                            'required' => true
                        ]
                    ]
                ],
                'response' => [
                    'data' => [
                        'access_token' => [
                            'type' => 'string',
                        ],
                        'expires_in' => [
                            'type' => 'integer',
                        ],
                        'token_type' => [
                            'type' => 'string'
                        ],
                        'scope' => [
                            'type' => 'string'
                        ],
                        'refresh_token' => [
                            'type' => 'string'
                        ]
                    ],
                    'example' => [
                        'access_token' => '5d082ff3a1fd48bdace77e4edd1ee219cc1735bd',
                        'expires_in' => 3600,
                        'token_type' => 'bearer',
                        'scope' => '',
                        'refresh_token' => '823606f6c584f80fd870d988e43c0d0e28720f41',
                    ]
                ]
            ],
            'user' => [
                'name' => 'Dados do usuário',
                'description' => 'Dados do usuário dono do Token enviado',
                'resource' => '/auth/user?access_token=&lt;access_token&gt;',
                'request' => [
                    'method' => 'GET',
                    'data' => []
                ],
                'response' => [
                    'data' => [
                        'id' => [
                            'type' => 'integer',
                        ],
                        'active' => [
                            'type' => 'boolean',
                        ],
                        'firstName' => [
                            'type' => 'string'
                        ],
                        'lastName' => [
                            'type' => 'string'
                        ],
                        'email' => [
                            'type' => 'string',
                        ],
                        'phone' => [
                            'type' => 'string'
                        ],
                        'username' => [
                            'type' => 'string',
                        ],
                        'role' => [
                            'type' => 'object',
                            'structure' => [
                                'id' => [
                                    'type' => 'integer',
                                ],
                                'name' => [
                                    'type' => 'string',
                                ],
                                'label' => [
                                    'type' => 'string',
                                ],
                                'admin' => [
                                    'type' => 'boolean',
                                ],
                            ]
                        ],
                        'language' => [
                            'type' => 'object',
                            'structure' => [
                                'id' => [
                                    'type' => 'integer',
                                ],
                                'locale' => [
                                    'type' => 'string',
                                ],
                                'name' => [
                                    'type' => 'string',
                                ],
                                'nativeName' => [
                                    'type' => 'string',
                                ],
                            ]
                        ],
                    ],
                    'example' => [
                        'id' => 1234,
                        'active' => 1,
                        'firstName' => 'John',
                        'lastName' => 'Smith',
                        'email' => 'jonhsmith@example.com',
                        'phone' => '+55 (27) 5555-9876',
                        'username' => 'johnsmith',
                        'role' => [
                            'id' => 1,
                            'name' => 'registered',
                            'label' => 'Usuario registrado',
                            'admin' => 0
                        ],
                        'language' => [
                            'id' => 2,
                            'name' => 'Ingles',
                            'nativeName' => 'English',
                            'locale' => 'en_US',
                        ],
                    ]
                ]
            ]
        ];

        $view = new ViewModel(['documentation' => $documentation]);
        $view->setTemplate('doc');
        return $view;
    }

}
