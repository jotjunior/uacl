<?php

namespace Uacl\Controller;

use Zend\View\Model\JsonModel;

class NewPasswordController extends AbstractBaseController
{

    protected $entity = 'Uacl\Entity\User';

    public function create($data)
    {

        if (strlen($data['password']) < 6) {
            return new JsonModel(array('success' => 0, 'error' => 'A senha deve possuir no mínimo 6 caracteres', 'errorCode' => 'INVALID_PASSWORD'));
        }

        // verifica se o usuário existe
        $user = $this->getEm()
                ->getRepository('Uacl\Entity\User')
                ->findOneBy(array('activationKey' => $data['activationKey']));

        if (!$user) {
            return new JsonModel(array('success' => 0, 'error' => 'Chave de ativação inválida ou inexistente. Favor realizar nova solicitação de senha.', 'errorCode' => 'INVALID_ACTIVATION_KEY'));
        }

        try {
            $user->setPassword($data['password']);
            $user->setActivationKey(null);
            $this->getEm()->persist($user);
            $this->getEm()->flush();
        } catch (\RuntimeException $e) {
            return new JsonModel(array('success' => 0, 'error' => $e->getMessage()));
        }

        return new JsonModel(array('success' => 1, 'data' => array('firstName' => $user->getFirstName(), 'email' => $user->getEmail())));
    }

}
