<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Uacl\ElasticSearch;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

/**
 * Description of AbstractSynchronizerFactory
 *
 * @author jot
 */
class AbstractSynchronizerFactory implements FactoryInterface
{

    protected $documentService;
    protected $resource;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        
        $this->documentService = $serviceLocator->get('MCNElasticSearch\Service\DocumentService');
        return new $this->resource($this->documentService);
    }

}
