<?php

/**
 * Módulo UACL
 * Responsavel pelo registro, validação e verificação de permissões dos usuários
 *
 * @copyright Copyright (c) 2014 Jot (http://www.jot.com,br)
 * @author João G. "Jot!" Zanon Jr. <jot@jot.com.br>
 */

namespace Uacl;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;


class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        // transformando erros de PHP em excecoes
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        set_error_handler(['Uacl\Module', 'handlePhpErrors']);

        // traduzindo os textos para pt_BR
        \Locale::setDefault('pt_BR');

//        $translator = $e->getApplication()->getServiceManager()->get('translator');
//        $translator->addTranslationFile(
//            'phpArray',
//            'vendor/zendframework/zend-i18n-resources/languages/pt_BR/Zend_Validate.php',
//            'default',
//            'pt_BR'
//        );
//        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);


    }

    /**
     * Captura os erros nativos do PHP e o transformam em exceções
     * @param int $type
     * @param string $message
     * @param string $file
     * @param int $line
     * @return void
     * @throws \Exception
     */
    public static function handlePhpErrors($type, $message, $file, $line)
    {
        if (!($type & error_reporting())) {
            return;
        }

        throw new \Exception('Erro: ' . $message . ' no arquivo ' . $file . ' na linha ' . $line);
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                /**
                 * Cache (Memcached)
                 */
                'Cache' => 'Uacl\Service\Cache',
                'doctrine.cache.uacl.doctrine.cache' => 'Uacl\Service\DoctrineCache',
                /**
                 * Serviço do Logger
                 */
                'Log' => 'Uacl\Logger\Log',
                /**
                 * Serviços da ACL
                 */
                'Permissions' => 'Uacl\Auth\Permission\Acl',
                /**
                 * Hydrator do Doctrine
                 */
                'Hydrator' => 'Uacl\Service\Hydrator',
                /**
                 * Services de persistência
                 */
                'Uacl\Service\RegisterPrivileges' => 'Uacl\Service\RegisterPrivileges',
                'Uacl\Service\Role' => 'Uacl\Service\Role',
                'Uacl\Service\Resource' => 'Uacl\Service\Resource',
                'Uacl\Service\Privilege' => 'Uacl\Service\Privilege',
                'Uacl\Service\Menu' => 'Uacl\Service\Menu',
                'Uacl\Service\User' => 'Uacl\Service\User',
                'Uacl\Service\Language' => 'Uacl\Service\Language',
                'Uacl\Service\Client' => 'Uacl\Service\Client',
                /**
                 * Services do oAuth
                 */
                'Uacl\UserCredentials' => 'Uacl\Auth\Storage\UserCredentialsStorage',
                'Uacl\AccessToken' => 'Uacl\Auth\Storage\AccessTokenStorage',
                'Uacl\ClientCredentials' => 'Uacl\Auth\Storage\ClientCredentialsStorage',
                'Uacl\AuthorizationCode' => 'Uacl\Auth\Storage\AuthorizationCodeStorage',
                'Uacl\RefreshToken' => 'Uacl\Auth\Storage\RefreshTokenStorage',
                /**
                 * Classes do Gearman
                 */
                'Uacl\Gearman\Worker' => 'Uacl\Service\GearmanWorker',
                'Uacl\Gearman\Client' => 'Uacl\Service\GearmanClient',
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
        );
    }

}
