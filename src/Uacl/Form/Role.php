<?php

namespace Uacl\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Form\Element\ObjectSelect as DoctrineObjectSelect;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class Role extends Form
{

    public function __construct(EntityManager $em)
    {

        parent::__construct('role');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new RoleFilter($em));

        $id = new Element\Hidden;
        $id->setName('id')
                ->setAttribute('id', 'role-id');
        $this->add($id);

        // NAME
        $name = new Element\Text;
        $name->setName('name')
                ->setLabel('Nome')
                ->setAttribute('id', 'role-name');
        $this->add($name);

        // LABEL
        $label = new Element\Text;
        $label->setName('label')
                ->setLabel('Legenda')
                ->setAttribute('id', 'role-label');
        $this->add($label);

        // ADMIN
        $admin = new Element\Radio;
        $admin->setName('admin')
                ->setLabel('Is admin?')
                ->setAttribute('id', 'role-admin')
                ->setOptions(array(
                    'value_options' => array(false => 'no', true => 'yes',),
        ));
        $this->add($admin);

        // PARENT
        $parent = new DoctrineObjectSelect;
        $parent->setName('parent')
                ->setLabel('Parent')
                ->setAttribute('id', 'role-parent')
                ->setOptions(array(
                    'object_manager' => $em,
                    'target_class' => 'Uacl\Entity\Role',
                    'property' => 'name',
                    'is_method' => true,
                    'empty_option' => 'Selecione um papel',
                    'find_method' => array(
                        'name' => 'findBy',
                        'params' => array(
                            'criteria' => array(),
                            'orderBy' => array('id' => 'ASC'),
                        ),
                    ),
        ));
        $this->add($parent);

        // SUBMIT
        $field = new Element\Submit;
        $field->setName('submit')
                ->setAttributes(array(
                    'value' => 'Salvar',
                    'id' => 'role-submit',
        ));
        $this->add($field);
    }

}
