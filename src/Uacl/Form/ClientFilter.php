<?php

namespace Uacl\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ClientFilter extends InputFilter
{

    public function __construct()
    {

        $factory = new InputFactory();

        // ID
        $this->add($factory->createInput([
                    'name' => 'id',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Digits'),
                    ),
        ]));

        // USER
        $this->add($factory->createInput([
                    'name' => 'user',
                    'required' => false,
                    'validators' => array(
                        array(
                            'name' => 'DoctrineModule\Validator\ObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\User'),
                                'fields' => array('id')
                            )
                        ),
                    ),
        ]));


        // CLIENT ID
        $this->add($factory->createInput([
                    'name' => 'clientKey',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '16',
                                'max' => '32',
                            ),
                        ),
                    ),
        ]));


        // CLIENT SECRET
        $this->add($factory->createInput([
                    'name' => 'clientSecret',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '16',
                                'max' => '32',
                            ),
                        ),
                    ),
        ]));


        // REDIRECT URI
        $this->add($factory->createInput([
                    'name' => 'redirectUri',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '10',
                                'max' => '2000',
                            ),
                        ),
                    ),
        ]));


        // GRANT TYPES
        $this->add($factory->createInput([
                    'name' => 'grantTypes',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '10',
                                'max' => '2000',
                            ),
                        ),
                    ),
        ]));
    }

}
