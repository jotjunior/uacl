<?php

namespace Uacl\Form;

use Doctrine\ORM\EntityManager;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class PrivilegeFilter extends InputFilter
{

    public function __construct(EntityManager $em)
    {

        $factory = new InputFactory();

        // ID
        $this->add($factory->createInput([
                    'name' => 'id',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Digits'),
                    ),
        ]));

        // RESOURCE
        $this->add($factory->createInput([
                    'name' => 'resource',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'DoctrineModule\Validator\ObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\Resource'),
                                'fields' => array('id')
                            )
                        ),
                    ),
        ]));

        // ROLE
        $this->add($factory->createInput([
                    'name' => 'role',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'DoctrineModule\Validator\ObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\Role'),
                                'fields' => array('id')
                            )
                        ),
                    ),
        ]));

        // ACTION
        $this->add($factory->createInput([
                    'name' => 'action',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '50',
                            ),
                        ),
                    ),
        ]));
    }

}
