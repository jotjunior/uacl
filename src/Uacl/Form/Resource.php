<?php

namespace Uacl\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;

class Resource extends Form
{

    /**
     * @var InputFilter 
     */
    protected $inputfilter;

    public function __construct()
    {

        parent::__construct('resource');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new ResourceFilter());

        // ID
        $id = new Element\Hidden;
        $id->setName('id')
                ->setAttribute('id', 'resource-id');
        $this->add($id);

        // NAME
        $name = new Element\Text;
        $name->setName('name')
                ->setLabel('Controlador')
                ->setAttribute('id', 'resource-name');
        $this->add($name);

        // SHORT NAME
        $shortName = new Element\Text;
        $shortName->setName('shortName')
                ->setLabel('Controlador (nome curto)')
                ->setAttribute('id', 'resource-short-name');
        $this->add($shortName);

        // MODULE
        $module = new Element\Text;
        $module->setName('module')
                ->setLabel('Module')
                ->setAttribute('id', 'resource-module');
        $this->add($module);

        // SUBMIT
        $submit = new Element\Submit;
        $submit->setName('submit')
                ->setValue('save')
                ->setAttribute('id', 'resource-submit');
        $this->add($submit);
    }

}
