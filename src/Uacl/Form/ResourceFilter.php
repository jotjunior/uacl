<?php

namespace Uacl\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class ResourceFilter extends InputFilter
{

    public function __construct()
    {

        $factory = new InputFactory();

        // ID
        $this->add($factory->createInput([
                    'name' => 'id',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Digits'),
                    ),
        ]));

        // NAME
        $this->add($factory->createInput([
                    'name' => 'name',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '200',
                            ),
                        ),
                    ),
        ]));

        // SHORT NAME
        $this->add($factory->createInput([
                    'name' => 'shortName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '50',
                            ),
                        ),
                    ),
        ]));

        // MODULE
        $this->add($factory->createInput([
                    'name' => 'module',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '50',
                            ),
                        ),
                    ),
        ]));
    }

}
