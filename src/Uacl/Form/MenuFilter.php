<?php

namespace Uacl\Form;

use Doctrine\ORM\EntityManager;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class MenuFilter extends InputFilter
{

    public function __construct(EntityManager $em)
    {

        $factory = new InputFactory();

        // ID
        $this->add($factory->createInput([
                    'name' => 'id',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Digits'),
                    ),
        ]));

        // PARENT
        $this->add($factory->createInput([
                    'name' => 'parent',
                    'required' => false,
                    'validators' => array(
                        array(
                            'name' => 'DoctrineModule\Validator\ObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\Menu'),
                                'fields' => array('id')
                            )
                        ),
                    ),
        ]));

        // RESOURCE
        $this->add($factory->createInput([
                    'name' => 'resource',
                    'required' => false,
                    'validators' => array(
                        array(
                            'name' => 'DoctrineModule\Validator\ObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\Resource'),
                                'fields' => array('id')
                            )
                        ),
                    ),
        ]));

        // NAME
        $this->add($factory->createInput([
                    'name' => 'name',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 2,
                                'max' => 50,
                            ),
                        ),
                    ),
        ]));

        // ROUTE
        $this->add($factory->createInput([
                    'name' => 'route',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 2,
                                'max' => 255,
                            ),
                        ),
                    ),
        ]));

        // ACTION
        $this->add($factory->createInput([
                    'name' => 'action',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 2,
                                'max' => 50,
                            ),
                        ),
                    ),
        ]));

        // CLASS
        $this->add($factory->createInput([
                    'name' => 'class',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'max' => 50,
                            ),
                        ),
                    ),
        ]));

        // DESCRIPTION
        $this->add($factory->createInput([
                    'name' => 'description',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
        ]));

        // POSITION
        $this->add($factory->createInput([
                    'name' => 'position',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Digits'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'Digits',
                        ),
                    ),
        ]));

        // VISIBLE
        $this->add($factory->createInput([
                    'name' => 'visible',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Boolean'),
                    ),
        ]));
    }

}
