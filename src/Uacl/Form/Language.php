<?php

namespace Uacl\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;

class Language extends Form
{

    /**
     * @var InputFilter 
     */
    protected $inputfilter;

    public function __construct()
    {

        parent::__construct('language');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new LanguageFilter());

        // ID
        $id = new Element\Hidden;
        $id->setName('id')
                ->setAttribute('id', 'language-id');
        $this->add($id);

        // NAME
        $name = new Element\Text;
        $name->setName('name')
                ->setLabel('Controlador')
                ->setAttribute('id', 'language-name');
        $this->add($name);

        // NATIVE NAME
        $nativeName = new Element\Text;
        $nativeName->setName('nativeName')
                ->setLabel('Nome original')
                ->setAttribute('id', 'language-native-name');
        $this->add($nativeName);

        // LOCALE
        $locale = new Element\Text;
        $locale->setName('locale')
                ->setLabel('Locale')
                ->setAttribute('id', 'language-locale');
        $this->add($locale);

        // SUBMIT
        $submit = new Element\Submit;
        $submit->setName('submit')
                ->setValue('Salvar')
                ->setAttribute('id', 'language-submit');
        $this->add($submit);
    }

}
