<?php

namespace Uacl\Form;

use Doctrine\ORM\EntityManager;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class UserFilter extends InputFilter
{

    public function __construct(EntityManager $em)
    {

        $factory = new InputFactory();

        // ID
        $this->add($factory->createInput([
                    'name' => 'id',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Digits'),
                    ),
        ]));

        // GRANT TYPES
        $this->add($factory->createInput([
                    'name' => 'grantTypes',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '100',
                            ),
                        ),
                    ),
        ]));

        // FIRST NAME
        $this->add($factory->createInput([
                    'name' => 'firstName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '100',
                            ),
                        ),
                    ),
        ]));

        // LAST NAME
        $this->add($factory->createInput([
                    'name' => 'lastName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '100',
                            ),
                        ),
                    ),
        ]));

        // EMAIL
        $this->add($factory->createInput([
                    'name' => 'email',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '200',
                            ),
                        ),
                        array(
                            'name' => 'EmailAddress',
                        ),
                        array(
                            'name' => 'NotEmpty',
                        ),
                        array(
                            'name' => 'DoctrineModule\Validator\NoObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\User'),
                                'fields' => array('email'),
                                'messages' => array(
                                    'objectFound' => 'Este e-mail pertence a outro usuário'
                                )
                            )
                        ),
                    ),
        ]));

        // PHONE
        $this->add($factory->createInput([
                    'name' => 'phone',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'max' => '20',
                            ),
                        ),
                    ),
        ]));


        // USERNAME
        $this->add($factory->createInput([
                    'name' => 'username',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'min' => '3',
                                'max' => '50',
                            ),
                        ),
                        array(
                            'name' => 'DoctrineModule\Validator\NoObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\User'),
                                'fields' => array('username'),
                                "messages" => array(
                                    "objectFound" => "Este nome de usuário já foi utilizado por outra pessoa."
                                )
                            )
                        ),
                    ),
        ]));

        // PASSWORD
        $this->add($factory->createInput([
                    'name' => 'password',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'min' => '6',
                                'max' => '30',
                            ),
                        ),
                    ),
        ]));


        // ACTIVE
        $this->add($factory->createInput([
                    'name' => 'active',
                    'required' => false,
        ]));

        // ROLE
        $this->add($factory->createInput([
                    'name' => 'role',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'DoctrineModule\Validator\ObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\Role'),
                                'fields' => array('id')
                            )
                        ),
                    ),
        ]));

        // LANGUAGE
        $this->add($factory->createInput([
                    'name' => 'language',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'DoctrineModule\Validator\ObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\Language'),
                                'fields' => array('id')
                            )
                        ),
                    ),
        ]));
    }

}
