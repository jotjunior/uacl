<?php

namespace Uacl\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Form\Element\ObjectSelect as DoctrineObjectSelect;

class User extends Form
{

    public function __construct(EntityManager $em)
    {

        parent::__construct('user');

        $this->setAttribute('method', 'post')
                ->setAttribute('class', 'new_user_form inline-input')
                ->setInputFilter(new UserFilter($em))
        ;

        // ID
        $id = new Element\Hidden;
        $id->setName('id')
                ->setAttribute('id', 'user-id');
        $this->add($id);

        // ACTIVE
        $active = new Element\Checkbox;
        $active->setName('active')
                ->setLabel('Ativo')
                ->setAttribute('id', 'user-active');
        $this->add($active);

        // FIRST NAME
        $firstName = new Element\Text;
        $firstName->setName('firstName')
                ->setLabel('Nome')
                ->setAttribute('id', 'user-first-name');
        $this->add($firstName);

        // LAST NAME
        $lastName = new Element\Text;
        $lastName->setName('lastName')
                ->setLabel('Sobrenome')
                ->setAttribute('id', 'user-name');
        $this->add($lastName);

        // USERNAME
        $password = new Element\Text;
        $password->setName('username')
                ->setLabel('Usuário')
                ->setAttributes(array(
                    'id' => 'user-username'
        ));
        $this->add($password);

        // PASSWORD
        $password = new Element\Password;
        $password->setName('password')
                ->setLabel('Senha')
                ->setAttributes(array(
                    'id' => 'user-password'
        ));
        $this->add($password);

        // PASSWORD CONFIRMATION
        $passwordConfirmation = new Element\Password;
        $passwordConfirmation->setName('password-confirmation')
                ->setLabel('Confirme a senha')
                ->setAttribute('id', 'user-password-confirmation');
        $this->add($passwordConfirmation);

        // EMAIL
        $email = new Element\Text;
        $email->setName('email')
                ->setLabel('E-mail')
                ->setAttribute('id', 'user-email');
        $this->add($email);

        // EMAIL CONFIRMATION
        $emailConfirmation = new Element\Text;
        $emailConfirmation->setName('email-confirmation')
                ->setLabel('Confirme o e-mail')
                ->setAttribute('id', 'user-email-confirmation');
        $this->add($emailConfirmation);

        // PHONE
        $phone = new Element\Text;
        $phone->setName('email-confirmation')
                ->setLabel('Confirme o e-mail')
                ->setAttribute('id', 'user-phone');
        $this->add($phone);

        // GRANT TYPES
        $grantTypes = new Element\Text;
        $grantTypes->setName('grantTypes')
                ->setLabel('Tipo de permissão')
                ->setAttribute('id', 'user-grant_type');
        $this->add($grantTypes);

        // DOCUMENTO
        $document = new Element\Text;
        $document->setName('document')
                ->setLabel('CPF')
                ->setAttribute('id', 'user-document');
        $this->add($document);

        // LANGUAGE
        $language = new DoctrineObjectSelect;
        $language->setName('language')
                ->setLabel('Language')
                ->setAttribute('id', 'user-language')
                ->setOptions(array(
                    'object_manager' => $em,
                    'target_class' => 'Uacl\Entity\Language',
                    'property' => 'nativeName',
                    'is_method' => true,
                    'find_method' => array(
                        'name' => 'findBy',
                        'params' => array(
                            'criteria' => array(),
                            'orderBy' => array('nativeName' => 'ASC'),
                        ),
                    ),
        ));
        $this->add($language);

        // ROLE
        $role = new DoctrineObjectSelect;
        $role->setName('role')
                ->setLabel('Papel')
                ->setAttribute('id', 'user-role')
                ->setOptions(array(
                    'object_manager' => $em,
                    'target_class' => 'Uacl\Entity\Role',
                    'property' => 'name',
                    'is_method' => true,
                    'find_method' => array(
                        'name' => 'findBy',
                        'params' => array(
                            'criteria' => array(),
                            'orderBy' => array('name' => 'ASC'),
                        ),
                    ),
        ));
        $this->add($role);

        // SUBMIT
        $submit = new Element\Button;
        $submit->setName('submit')
                ->setLabel('')
                ->setAttributes(array(
                    'id' => 'user-submit',
                    'type' => 'submit'
        ));
        $this->add($submit);
    }

}
