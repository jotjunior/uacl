<?php

namespace Uacl\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Form\Element\ObjectSelect as DoctrineObjectSelect;

class Privilege extends Form
{

    public function __construct(EntityManager $em)
    {

        parent::__construct('privilege');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new PrivilegeFilter($em));

        // ID
        $id = new Element\Hidden;
        $id->setName('id')
                ->setAttribute('id', 'privilege-id');
        $this->add($id);

        // ROLE
        $role = new DoctrineObjectSelect;
        $role->setName('role')
                ->setLabel('Role')
                ->setAttribute('id', 'privilege-role')
                ->setOptions(array(
                    'object_manager' => $em,
                    'target_class' => 'Uacl\Entity\Role',
                    'property' => 'name',
                    'is_method' => true,
                    'find_method' => array(
                        'name' => 'findBy',
                        'params' => array(
                            'criteria' => array(),
                            'orderBy' => array('id' => 'ASC'),
                        ),
                    ),
        ));
        $this->add($role);

        // RESOURCE
        $resource = new DoctrineObjectSelect;
        $resource->setName('resource')
                ->setLabel('Resource')
                ->setAttribute('id', 'privilege-resource')
                ->setOptions(array(
                    'object_manager' => $em,
                    'target_class' => 'Uacl\Entity\Resource',
                    'property' => 'name',
                    'is_method' => true,
                    'find_method' => array(
                        'name' => 'findBy',
                        'params' => array(
                            'criteria' => array(),
                            'orderBy' => array('name' => 'ASC'),
                        ),
                    ),
        ));
        $this->add($resource);

        // ACTION
        $action = new Element\Text;
        $action->setName('action')
                ->setLabel('Action')
                ->setAttribute('id', 'privilege-action');
        $this->add($action);

        // SUBMIT
        $submit = new Element\Submit;
        $submit->setName('submit')
                ->setAttributes(array(
                    'value' => 'Salvar',
                    'id' => 'privilege-submit',
        ));
        $this->add($submit);
    }

}
