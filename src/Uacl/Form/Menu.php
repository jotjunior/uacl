<?php

namespace Uacl\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Form\Element\ObjectSelect as DoctrineObjectSelect;

class Menu extends Form
{

    public function __construct(EntityManager $em)
    {

        parent::__construct('menu');

        // injetando o InputFilter
        $this->setInputFilter(new MenuFilter($em));

        $this->setAttribute('method', 'post');

        // ID
        $id = new Element\Hidden;
        $id->setName('id')
                ->setAttribute('id', 'menu-id');
        $this->add($id);

        // NAME
        $name = new Element\Text;
        $name->setName('name')
                ->setLabel('Name')
                ->setAttribute('id', 'menu-name');
        $this->add($name);

        // ROUTE
        $route = new Element\Text;
        $route->setName('route')
                ->setLabel('Route')
                ->setAttribute('id', 'menu-route');
        $this->add($route);

        // ACTION
        $action = new Element\Text;
        $action->setName('action')
                ->setLabel('Action')
                ->setAttribute('id', 'menu-action');
        $this->add($action);

        // DESCRIPTION
        $description = new Element\Text;
        $description->setName('description')
                ->setLabel('Description')
                ->setAttribute('id', 'menu-description');
        $this->add($description);

        // POSITION
        $position = new Element\Text;
        $position->setName('position')
                ->setLabel('Position')
                ->setAttribute('id', 'menu-position');
        $this->add($position);

        // VISIBLE
        $visible = new Element\Text;
        $visible->setName('visible')
                ->setLabel('Visible')
                ->setAttribute('id', 'menu-visible');
        $this->add($visible);

        // CLASS
        $class = new Element\Text;
        $class->setName('class')
                ->setLabel('Classe')
                ->setAttribute('id', 'menu-class');
        $this->add($class);

        // PARENT
        $parent = new DoctrineObjectSelect;
        $parent->setName('parent')
                ->setLabel('Parent')
                ->setAttribute('id', 'menu-parent')
                ->setOptions(array(
                    'object_manager' => $em,
                    'target_class' => 'Uacl\Entity\Menu',
                    'property' => 'name',
                    'is_method' => true,
                    'empty_option' => 'Menu Pai',
                    'find_method' => array(
                        'name' => 'findBy',
                        'params' => array(
                            'criteria' => array(),
                            'orderBy' => array('position' => 'ASC'),
                        ),
                    ),
        ));
        $this->add($parent);

        // RESOURCE
        $resource = new DoctrineObjectSelect;
        $resource->setName('resource')
                ->setLabel('Resource')
                ->setAttribute('id', 'menu-resource')
                ->setOptions(array(
                    'object_manager' => $em,
                    'target_class' => 'Uacl\Entity\Resource',
                    'property' => 'name',
                    'is_method' => true,
                    'empty_option' => 'Selecione um recurso',
                    'find_method' => array(
                        'name' => 'findBy',
                        'params' => array(
                            'criteria' => array(),
                            'orderBy' => array('name' => 'ASC'),
                        ),
                    ),
        ));
        $this->add($resource);

        // SUBMIT
        $submit = new Element\Submit;
        $submit->setName('submit')
                ->setAttributes(array(
                    'value' => 'Salvar',
                    'id' => 'menu-submit'
        ));
        $this->add($submit);
    }

}
