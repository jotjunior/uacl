<?php

namespace Uacl\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;

class Client extends Form
{

    /**
     * @var InputFilter 
     */
    protected $inputfilter;

    public function __construct()
    {

        parent::__construct('client');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new ClientFilter());

        // ID
        $id = new Element\Hidden;
        $id->setName('id')
                ->setAttribute('id', 'client-id');
        $this->add($id);

        // ROLE
        $role = new DoctrineObjectSelect;
        $role->setName('user')
                ->setLabel('Usuário')
                ->setAttribute('id', 'client-user')
                ->setOptions(array(
                    'object_manager' => $em,
                    'target_class' => 'Uacl\Entity\User',
                    'property' => 'firstName',
                    'is_method' => true,
                    'find_method' => array(
                        'name' => 'findBy',
                        'params' => array(
                            'criteria' => array(),
                            'orderBy' => array('firstName' => 'ASC'),
                        ),
                    ),
        ));
        $this->add($role);

        // CLIENT ID
        $clientKey = new Element\Text;
        $clientKey->setName('clientKey')
                ->setLabel('Client Id')
                ->setAttribute('id', 'client-client_key');
        $this->add($clientKey);

        // CLIENT SECRET
        $clientSecret = new Element\Text;
        $clientSecret->setName('clientSecret')
                ->setLabel('Client Secret')
                ->setAttribute('id', 'client-client_secret');
        $this->add($clientSecret);

        // REDIRECT URI
        $redirectUri = new Element\Text;
        $redirectUri->setName('redirectUri')
                ->setLabel('Redirect URI')
                ->setAttribute('id', 'client-redirect_uri');
        $this->add($redirectUri);

        // GRANT TYPES
        $redirectUri = new Element\Text;
        $redirectUri->setName('grantTypes')
                ->setLabel('Grant Types')
                ->setAttribute('id', 'client-grant_types');
        $this->add($redirectUri);

        // SUBMIT
        $submit = new Element\Submit;
        $submit->setName('submit')
                ->setValue('save')
                ->setAttribute('id', 'client-submit');
        $this->add($submit);
    }

}
