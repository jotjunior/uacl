<?php

namespace Uacl\Form;

use Doctrine\ORM\EntityManager;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class RoleFilter extends InputFilter
{

    public function __construct(EntityManager $em)
    {

        $factory = new InputFactory();

        // ID
        $this->add($factory->createInput([
                    'name' => 'id',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Digits'),
                    ),
        ]));

        // NAME
        $this->add($factory->createInput([
                    'name' => 'name',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '50',
                            ),
                        ),
                    ),
        ]));

        // LABEL
        $this->add($factory->createInput([
                    'name' => 'label',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '50',
                            ),
                        ),
                    ),
        ]));

        // PARENT
        $this->add($factory->createInput([
                    'name' => 'parent',
                    'required' => false,
                    'validators' => array(
                        array(
                            'name' => 'DoctrineModule\Validator\ObjectExists',
                            'options' => array(
                                'object_repository' => $em->getRepository('Uacl\Entity\Role'),
                                'fields' => 'id'
                            )
                        ),
                    ),
        ]));
    }

}
