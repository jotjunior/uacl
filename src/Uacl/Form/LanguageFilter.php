<?php

namespace Uacl\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class LanguageFilter extends InputFilter
{

    public function __construct()
    {

        $factory = new InputFactory();

        // ID
        $this->add($factory->createInput([
                    'name' => 'id',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Digits'),
                    ),
        ]));

        // NAME
        $this->add($factory->createInput([
                    'name' => 'name',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '200',
                            ),
                        ),
                    ),
        ]));

        // NATIVE NAME
        $this->add($factory->createInput([
                    'name' => 'nativeName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '2',
                                'max' => '50',
                            ),
                        ),
                    ),
        ]));

        // LOCALE
        $this->add($factory->createInput([
                    'name' => 'locale',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => '5',
                                'max' => '10',
                            ),
                        ),
                    ),
        ]));
    }

}
