<?php

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => [
                    'host' => 'localhost',
                    'port' => '5432',
                    'user' => 'your_user',
                    'password' => 'your_pass',
                    'dbname' => 'you_dbname',
                    'driverOptions' => [
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
                    ],
                ]
            ]
        ],
        'configuration' => [
            'orm_default' => [
                'metadata_cache' => 'uacl.doctrine.cache',
                'query_cache' => 'uacl.doctrine.cache',
                'result_cache' => 'uacl.doctrine.cache',
            ]
        ],
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    /**
                     * Eventos do sincronizador
                     */
                ]
            ]
        ],
        
    ]
];
