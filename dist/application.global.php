<?php

/**
 * SALT da aplicação, usado na criptografia das senhas
 */
define('UACL_SALT', 'yG#:Il9 @|YSqVbn~fjm,r`.WFF6o%|=LT0mEO]JxNVB7XJ<U+Y2rtWM{<9Es|rz');

/**
 * Caso seja necessário trabalhar com o MICROSOFT SQL SERVER, descomente as 
 * linhas abaixo
 */
//use Doctrine\DBAL\Types\Type;
//Type::overrideType('datetime', 'Doctrine\DBAL\Types\VarDateTimeType');
//Type::overrideType('datetimetz', 'Doctrine\DBAL\Types\VarDateTimeType');
//Type::overrideType('time', 'Doctrine\DBAL\Types\VarDateTimeType');

return array(
    'application' => array(
        'name' => 'Nome do Aplicativo',
        'description' => 'Descrição do aplicativo',
        'url' => 'http://app.exemplo.com',
        'logo' => 'public/img/logo.png',
        'version' => array(
            'major' => 1,
            'minor' => 0,
            'bugfix' => 0
        ),
        'acl' => array(
            'enabled' => true,
            'public_controllers' => array(
                'Uacl\Controller\Auth',
                'Uacl\Controller\ResetPassword',
                'Uacl\Controller\NewPassword',
                'Application\Controller\Index',
            ),
            'public_actions' => array(
                'login'
            ),
        ),
        'options' => array(
            'allow_change_items_count_per_page' => true,
        ),
        'cache' => array(
            'adapter' => array(
                'name' => 'Memcached',
                'options' => array(
                    'ttl' => 3600,
                    'servers' => array(
                        array('127.0.0.1', 11211)
                    )
                )
            ),
            'plugins' => array(
                'Serializer',
                'exception_handler' => array('throw_exceptions' => true)
            )
        ),
    ),
);
