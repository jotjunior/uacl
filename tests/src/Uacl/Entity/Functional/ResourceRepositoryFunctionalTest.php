<?php

namespace Uacl\Entity;

use TestBase\Test\TestCase;

class ResourceRepositoryFunctionalTest extends TestCase {

	protected $module = 'Uacl';
	protected $requireDb = true;

	/**
	 * Cadastra todos os itens necessarios no banco de dados para testar o metodo
	 */
	public function testTestaMetodoGetlist() {

		$resource = new \Uacl\Entity\Resource;
		$resource->setModule('jacl')
			->setName('UmControllerQualquer')
			->setShortName('controller');
		$this->getEm()->persist($resource);

		$this->getEm()->flush();

		$list = $this->getEm()->getRepository('Uacl\Entity\Resource')->getList();

		$this->assertEquals('UmControllerQualquer', $list[1]);
	}

}
