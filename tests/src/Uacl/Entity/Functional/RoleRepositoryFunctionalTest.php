<?php

namespace Uacl\Entity;

use TestBase\Test\TestCase;

class RoleRepositoryFunctionalTest extends TestCase {

	protected $module = 'Uacl';
	protected $requireDb = true;

	/**
	 * Cadastra todos os itens necessarios no banco de dados para testar o metodo
	 */
	public function testTestaMetodoGetlist() {

		$role = new \Uacl\Entity\Role;
		$role->setName('admin')
			->setAdmin(true)
			->setLabel('Admin')
			->setParent(null);
		$this->getEm()->persist($role);

		$this->getEm()->flush();

		$list = $this->getEm()->getRepository('Uacl\Entity\Role')->getList();

		$this->assertEquals('admin', $list[1]['name']);
	}

}
