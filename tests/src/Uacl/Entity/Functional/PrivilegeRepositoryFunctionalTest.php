<?php

namespace Uacl\Entity;

use TestBase\Test\TestCase;

class PrivilegeRepositoryFunctionalTest extends TestCase {

	protected $module = 'Uacl';
	protected $requireDb = true;

	/**
	 * Cadastra todos os itens necessarios no banco de dados para testar o metodo
	 */
	public function testTestaMetodoGetlist() {

		$role = new \Uacl\Entity\Role;
		$role->setName('admin')
			->setAdmin(true)
			->setLabel('Admin')
			->setParent(null);
		$this->getEm()->persist($role);

		$resource = new \Uacl\Entity\Resource;
		$resource->setModule('jacl')
			->setName('UmControllerQualquer')
			->setShortName('controller');
		$this->getEm()->persist($resource);

		$privilege = new \Uacl\Entity\Privilege;
		$privilege->setAction('new')
			->setRole($role)
			->setResource($resource);
		$this->getEm()->persist($privilege);

		$this->getEm()->flush();

		$list = $this->getEm()->getRepository('Uacl\Entity\Privilege')->getList();

		$this->assertArrayHasKey('role', $list[1]);
	}

}
