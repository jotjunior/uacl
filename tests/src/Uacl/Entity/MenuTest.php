<?php

namespace Uacl\Entity;

use TestBase\Entity\EntityTestCase;

class MenuTest extends EntityTestCase {

	protected $module = 'Uacl';
	protected $entity = '\Uacl\Entity\Menu';

	/**
	 * Lista dos atributos da classe com valores padrão 
	 * Este método faz testes automáticos para as segintes situações:
	 *  - Testa se há todos os aributos
	 *  - Testa se há todos os getters e setters
	 *  - Testa se a classe possui interface fluente
	 * Casos de atributos que possuem somente setters ou getters devem ser
	 * testados separadamente.
	 * @return array
	 */
	public function dataProviderAttributes() {
		return array(
			array('id', 1),
			array('parent', null),
			array('parent', new \Uacl\Entity\Menu),
			array('resource', new \Uacl\Entity\Resource),
			array('name', 'Homne'),
			array('route', 'resource/list'),
			array('action', 'index'),
			array('class', 'icon-menu'),
			array('position', 1),
			array('visible', true),
			array('description', 'Um novo item do menu'),
		);
	}

}
