<?php

namespace Uacl\Entity;

use TestBase\Entity\EntityTestCase;

class AccessTokenTest extends EntityTestCase {

	protected $module = 'Uacl';
	protected $entity = '\Uacl\Entity\AccessToken';

	/**
	 * Lista dos atributos da classe com valores padrão 
	 * Este método faz testes automáticos para as segintes situações:
	 *  - Testa se há todos os aributos
	 *  - Testa se há todos os getters e setters
	 *  - Testa se a classe possui interface fluente
	 * Casos de atributos que possuem somente setters ou getters devem ser
	 * testados separadamente.
	 * @return array
	 */
	public function dataProviderAttributes() {
		return array(
			array('id', md5('abcdefghijklmn')),
			array('client', new \Uacl\Entity\Client),
			array('user', new \Uacl\Entity\User),
			array('expires', (new \DateTime())->getTimestamp()),
		);
	}

}
