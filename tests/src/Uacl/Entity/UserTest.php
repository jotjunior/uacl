<?php

namespace Uacl\Entity;

use TestBase\Entity\EntityTestCase;

class UserTest extends EntityTestCase {

	protected $module = 'Uacl';
	protected $entity = '\Uacl\Entity\User';

	/**
	 * Lista dos atributos da classe com valores padrão 
	 * Este método faz testes automáticos para as segintes situações:
	 *  - Testa se há todos os aributos
	 *  - Testa se há todos os getters e setters
	 *  - Testa se a classe possui interface fluente
	 * Casos de atributos que possuem somente setters ou getters devem ser
	 * testados separadamente.
	 * @return array
	 */
	public function dataProviderAttributes() {
		return array(
			array('id', 1),
			array('firstName', 'Severino '),
			array('lastName', 'Severino Bezerra '),
			array('email', 'severino@cangaco.com.br'),
			array('phone', '2690900909'),
			array('role', new \Uacl\Entity\Role),
			array('language', new \Uacl\Entity\Language),
			array('activationKey', '8a6f789ds6f9a78dsf6a78dsf798sd6'),
			array('updated', new \DateTime('now')),
			array('grantTypes', 'password'),
			array('active', true),
		);
	}

	public function testVerificaSeGetCreatedExiste() {
		$class = new $this->entity;
		$date = $class->getCreated();
		$this->assertInstanceOf('\Datetime', $date);
	}

	public function testVerificaGetterESetterDoPassword() {
		$class = new $this->entity;
		$class->setPassword('23456');
		$encryptedPassword = $class->encryptPassword('23456');
		$this->assertEquals($encryptedPassword, $class->getPassword());
	}

}
