<?php

namespace Uacl\Entity;

use TestBase\Entity\EntityTestCase;

class LanguageTest extends EntityTestCase {

	protected $module = 'Uacl';
	protected $entity = '\Uacl\Entity\Language';

	/**
	 * Lista dos atributos da classe com valores padrão 
	 * Este método faz testes automáticos para as segintes situações:
	 *  - Testa se há todos os aributos
	 *  - Testa se há todos os getters e setters
	 *  - Testa se a classe possui interface fluente
	 * Casos de atributos que possuem somente setters ou getters devem ser
	 * testados separadamente.
	 * @return array
	 */
	public function dataProviderAttributes() {
		return array(
			array('id', 1),
			array('name', 'Português'),
			array('nativeName', 'Português'),
			array('locale', 'pt_BR'),
		);
	}

}
