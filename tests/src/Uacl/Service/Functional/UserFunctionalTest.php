<?php

namespace Uacl\Service;

use TestBase\Service\ServiceFunctionalTestCase;

class UserFunctionalTest extends ServiceFunctionalTestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';

	/**
	 * Nome do serviço que está sendo testado
	 * @var string
	 */
	protected $service = '\Uacl\Service\User';

	/**
	 * Nome da Entidade que será injetada no serviço para testar o módulo
	 * @var string
	 */
	protected $entity = '\Uacl\Entity\User';

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaInsercaoNoBancoDeDados() {
		$role = new \Uacl\Entity\Role;
		$role->setName('admin')
			->setAdmin(true)
			->setLabel('Admin')
			->setParent(null);
		$this->getEm()->persist($role);
		$this->getEm()->flush();

		$language = new \Uacl\Entity\Language;
		$language->setLocale('pt_BR')
			->setName('Português')
			->setNativeName('Português');
		$this->getEm()->persist($language);
		$this->getEm()->flush();

		$this->entityData = array(
			'firstName' => 'Fulano',
			'lastName' => 'de Tal',
			'email' => 'fulano@bol.com.br',
			'activationKey' => md5(microtime()),
			'phone' => '123456789',
			'updated' => null,
			'password' => '123456',
			'role' => 1,
			'language' => 1,
			'grantTypes' => 'password',
			'active' => true,
		);

		parent::testExecutaInsercaoNoBancoDeDados();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaAlteracaoNoBancoDeDados() {

		$role = new \Uacl\Entity\Role;
		$role->setName('admin')
			->setAdmin(true)
			->setLabel('Admin')
			->setParent(null);
		$this->getEm()->persist($role);

		$language = new \Uacl\Entity\Language;
		$language->setLocale('pt_BR')
			->setName('Português')
			->setNativeName('Português');
		$this->getEm()->persist($language);

		$this->getEm()->flush();

		// inserindo um user para atribuí-lo ao user
		$user = new \Uacl\Entity\User;
		$user->setFirstName('Fulano')
			->setLastName('de Tal')
			->setEmail('fulano@bol.com.br')
			->setActivationKey(md5(microtime()))
			->setPassword('123456')
			->setPhone('123456')
			->setUpdated(null)
			->setActive(true)
			->setRole($role)
			->setGrantTypes('password')
			->setLanguage($language);
		$this->getEm()->persist($user);

		$this->getEm()->flush();

		$this->entityData = array(
			'id' => 1,
			'email' => 'fulano@bol.com.br',
			'activationKey' => md5(microtime()),
			'updated' => new \DateTime,
		);

		parent::testExecutaAlteracaoNoBancoDeDados();

		$this->assertInstanceOf('\Datetime', $user->getCreated());
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaRemocaoNoBancoDeDados() {
		$this->entityData = array(
			'id' => 1,
		);

		parent::testExecutaRemocaoNoBancoDeDados();
	}

}
