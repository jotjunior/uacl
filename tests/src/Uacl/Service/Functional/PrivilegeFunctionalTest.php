<?php

namespace Uacl\Service;

use TestBase\Service\ServiceFunctionalTestCase;

class PrivilegeFunctionalTest extends ServiceFunctionalTestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';

	/**
	 * Nome do serviço que está sendo testado
	 * @var string
	 */
	protected $service = 'Uacl\Service\Privilege';

	/**
	 * Nome da Entidade que será injetada no serviço para testar o módulo
	 * @var string
	 */
	protected $entity = '\Uacl\Entity\Privilege';

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaInsercaoNoBancoDeDados() {

		$resource = new \Uacl\Entity\Resource;
		$resource->setModule('jacl')
				->setName('Uacl\Controller\Resource')
				->setShortName('resource');
		$this->getEm()->persist($resource);
		$this->getEm()->flush();

		$role = new \Uacl\Entity\Role;
		$role->setName('guest')
				->setAdmin(false)
				->setLabel('Visitante')
				->setParent(null);
		$this->getEm()->persist($role);
		$this->getEm()->flush();


		$this->entityData = array(
			'resource' => 1,
			'role' => 1,
			'action' => 'index',
		);

		parent::testExecutaInsercaoNoBancoDeDados();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaAlteracaoNoBancoDeDados() {

		$resource = new \Uacl\Entity\Resource;
		$resource->setModule('jacl')
				->setName('Uacl\Controller\Resource')
				->setShortName('resource');
		$this->getEm()->persist($resource);
		$this->getEm()->flush();

		$role = new \Uacl\Entity\Role;
		$role->setName('guest')
				->setAdmin(false)
				->setLabel('Visitante')
				->setParent(null);
		$this->getEm()->persist($role);
		$this->getEm()->flush();

		$privilege = new \Uacl\Entity\Privilege;
		$privilege->setResource($resource)
				->setRole($role)
				->setAction('edit');
		$this->getEm()->persist($privilege);
		$this->getEm()->flush();

		$this->entityData = array(
			'id' => 1,
			'resource' => 1,
			'role' => 1,
			'action' => 'new',
		);

		parent::testExecutaAlteracaoNoBancoDeDados();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaRemocaoNoBancoDeDados() {
		$this->entityData = array(
			'id' => 1,
		);

		parent::testExecutaRemocaoNoBancoDeDados();
	}

}
