<?php

namespace Uacl\Service;

use TestBase\Service\ServiceFunctionalTestCase;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class MenuFunctionalTest extends ServiceFunctionalTestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';

	/**
	 * Nome do serviço que está sendo testado
	 * @var string
	 */
	protected $service = '\Uacl\Service\Menu';

	/**
	 * Nome da Entidade que será injetada no serviço para testar o módulo
	 * @var string
	 */
	protected $entity = '\Uacl\Entity\Menu';

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaInsercaoNoBancoDeDados() {

		// inserindo um resource para atribuí-lo ao menu
		$resource = new \Uacl\Entity\Resource;
		$resource->setModule('uacl')
				->setName('Uacl\Controller\Resource')
				->setShortName('resource');
		$this->getEm()->persist($resource);
		$this->getEm()->flush();

		$this->entityData = array(
			'parent' => null,
			'resource' => 1,
			'name' => 'Recursos',
			'route' => 'api-route',
			'action' => 'get',
			'description' => 'Este é o menu',
			'position' => 3,
			'visible' => true
		);
		parent::testExecutaInsercaoNoBancoDeDados();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaAlteracaoNoBancoDeDados() {

		// inserindo um resource para atribuí-lo ao menu
		$resource = new \Uacl\Entity\Resource;
		$resource->setModule('uacl')
				->setName('Uacl\Controller\Resource')
				->setShortName('resource');
		$this->getEm()->persist($resource);
		$this->getEm()->flush();

		// inserindo um menu para fazer sua alteracao em seguida
		$menu = new \Uacl\Entity\Menu;
		$menu->setParent(null)
				->setResource($resource)
				->setName('Recursos')
				->setRoute('api-route')
				->setAction('get')
				->setDescription('Este é o menu')
				->setPosition(3)
				->setVisible(true);
		$this->getEm()->persist($menu);
		$this->getEm()->flush();

		$this->entityData = array(
			'id' => 1,
			'parent' => null,
			'resource' => 1,
			'name' => 'Recursos alterados',
			'route' => 'api-route',
			'action' => 'get',
			'description' => 'Este é o menu',
			'position' => 3,
			'visible' => true
		);

		parent::testExecutaAlteracaoNoBancoDeDados();
	}
	
	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaRemocaoNoBancoDeDados() {
		$this->entityData = array(
			'id' => 1,
		);
		
		parent::testExecutaRemocaoNoBancoDeDados();
	}

}
