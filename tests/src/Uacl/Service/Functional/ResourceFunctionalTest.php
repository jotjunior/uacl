<?php

namespace Uacl\Service;

use TestBase\Service\ServiceFunctionalTestCase;

class ResourceFunctionalTest extends ServiceFunctionalTestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';

	/**
	 * Nome do serviço que está sendo testado
	 * @var string
	 */
	protected $service = '\Uacl\Service\Resource';

	/**
	 * Nome da Entidade que será injetada no serviço para testar o módulo
	 * @var string
	 */
	protected $entity = '\Uacl\Entity\Resource';

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaInsercaoNoBancoDeDados() {

		$this->entityData = array(
			'name' => 'Uacl\Controller\Role',
			'shortName' => 'role',
			'module' => 'jacl',
		);

		parent::testExecutaInsercaoNoBancoDeDados();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaAlteracaoNoBancoDeDados() {

		// inserindo um resource para atribuí-lo ao resource
		$resource = new \Uacl\Entity\Resource;
		$resource->setModule('jacl')
				->setName('Uacl\Controller\Resource')
				->setShortName('resource');
		$this->getEm()->persist($resource);
		$this->getEm()->flush();

		$this->entityData = array(
			'id' => 1,
			'name' => 'Uacl\Controller\Role',
			'shortName' => 'role',
			'module' => 'jacl',
		);

		parent::testExecutaAlteracaoNoBancoDeDados();
	}
	
	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaRemocaoNoBancoDeDados() {
		$this->entityData = array(
			'id' => 1,
		);
		
		parent::testExecutaRemocaoNoBancoDeDados();
	}

}
