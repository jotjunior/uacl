<?php

namespace Uacl\Service;

use TestBase\Service\ServiceFunctionalTestCase;

class RoleFunctionalTest extends ServiceFunctionalTestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';

	/**
	 * Nome do serviço que está sendo testado
	 * @var string
	 */
	protected $service = '\Uacl\Service\Role';

	/**
	 * Nome da Entidade que será injetada no serviço para testar o módulo
	 * @var string
	 */
	protected $entity = '\Uacl\Entity\Role';

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaInsercaoNoBancoDeDados() {

		$this->entityData = array(
			'parent' => null,
			'name' => 'guest',
			'admin' => false,
			'label' => 'Visitante'
		);

		parent::testExecutaInsercaoNoBancoDeDados();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaAlteracaoNoBancoDeDados() {

		// inserindo um role para atribuí-lo ao role
		$role1 = new \Uacl\Entity\Role;
		$role1->setParent(null)
				->setName('guest')
				->setAdmin(false)
				->setLabel('Visitante');
		$this->getEm()->persist($role1);
		$this->getEm()->flush();

		$role2 = new \Uacl\Entity\Role;
		$role2->setParent($role1)
				->setName('registered')
				->setAdmin(false)
				->setLabel('Registrado');
		$this->getEm()->persist($role2);
		$this->getEm()->flush();

		$this->entityData = array(
			'id' => 1,
			'parent' => 2,
			'name' => 'guest',
			'admin' => false,
			'label' => 'Visitante'
		);

		parent::testExecutaAlteracaoNoBancoDeDados();
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testExecutaRemocaoNoBancoDeDados() {
		$this->entityData = array(
			'id' => 1,
		);

		parent::testExecutaRemocaoNoBancoDeDados();
	}

}
