<?php

namespace Uacl\Service;

use TestBase\Service\ServiceTestCase;

class MenuTest extends ServiceTestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';

	/**
	 * Nome do serviço que está sendo testado
	 * @var string
	 */
	protected $service = '\Uacl\Service\Menu';

	/**
	 * Nome da Entidade que será injetada no serviço para testar o módulo
	 * @var string
	 */
	protected $entity = '\Uacl\Entity\Menu';

}
