<?php

namespace Uacl\Form;

use TestBase\Test\TestCase;

class LanguageTest extends TestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';
	
	public function testVerificaSeDadosEnviadosParaOFormSaoFiltradosEValidados() {
		
		$form = new \Uacl\Form\Language();
		
		// testa o envio de texto com tags HTML e com espaços em branco no início e no final
		$form->setData(array( 
			'name' => 'Português',
			'nativeName' => '<a>Português</a>',
			'locale' => ' pt_BR '
		));
		
		if($form->isValid()) {
			$data = $form->getData();
		}

		$this->assertEquals('Português', $data['nativeName']);
		$this->assertEquals('pt_BR', $data['locale']);
		
		
		// testa o retorno de mensagem do formulário para textos maiores do que o limit permitido
		$form->setData(array( 
			'name' => 'Português',
			'nativeName' => '<a>Potuguês</a>',
			'locale' => ' jacl askjdf ;alkdsjfa;kldfjsa;k ldsjfa;kkdfja ;dklsjf a;dskljf a;klsd jfa;kldsfjlsdjf a;klsdjf a;kdlsfj a;dsklfj a;dsklfj a;sdlkfj a;dsklfj a;ldfjs;kaljsdf;k'
		));
		
		if($form->isValid()) {
			$data = $form->getData();
		}
		
		$this->assertArrayHasKey('locale', $form->getMessages());
	}

}

