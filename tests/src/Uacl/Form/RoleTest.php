<?php

namespace Uacl\Form;

use TestBase\Test\TestCase;

class RoleTest extends TestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testVerificaSeDadosEnviadosParaOFormSaoFiltradosEValidados() {

		// inserindo uma Role para tetar o filter que testa a existência da classe
		$role = new \Uacl\Entity\Role;
		$role->setName('guest')
				->setParent(null)
				->setAdmin(false)
				->setLabel('Visitante');
		$this->getEm()->persist($role);
		$this->getEm()->flush();
		
		$form = new \Uacl\Form\Role($this->getEm());

		// no primeiro teste, verifica se o filter stripTags está funcionando
		$form->setData(array(
			'name' => 'registered',
			'admin' => 0,
			'label' => '<b>Registrado</b>',
			'parent' => 1
		));

		if ($form->isValid()) {
			$data = $form->getData();
		}
		$this->assertEquals('Registrado', $data['label']);

		// no segundo teste, verifica se o validator retorna uma mensagem por não encontrar o id na lista de roles
		$form->setData(array(
			'name' => 'registered',
			'admin' => 0,
			'label' => '<b>Registrado</b>',
			'parent' => 20
		));

		if ($form->isValid()) {
			$data = $form->getData();
		}

		$this->assertArrayHasKey('parent', $form->getMessages());
	}

}

