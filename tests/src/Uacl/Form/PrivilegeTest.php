<?php

namespace Uacl\Form;

use TestBase\Test\TestCase;

class PrivilegeTest extends TestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testVerificaSeDadosEnviadosParaOFormSaoFiltradosEValidados() {

		// inserindo um RESOURCE para tetar o filter que testa a existência da classe
		$resource = new \Uacl\Entity\Resource;
		$resource->setName('Jack\Controller\User')
				->setShortName('user')
				->setModule('jacl');
		$this->getEm()->persist($resource);

		// inserindo um ROLE para tetar o filter que testa a existência da classe
		$role = new \Uacl\Entity\Role;
		$role->setName('guest')
				->setLabel('Visitante')
				->setParent(null)
				->setAdmin(false);
		$this->getEm()->persist($role);

		$this->getEm()->flush();


		$form = new \Uacl\Form\Privilege($this->getEm());

		// no primeiro teste, verifica se o filter stripTags está funcionando
		// o primeiro teste verifica se consegue inserir tendo o PARENT como null
		$form->setData(array(
			'role' => 1,
			'resource' => 1,
			'action' => ' new '
		));

		if ($form->isValid()) {
			$data = $form->getData();
		}
		
		$this->assertEquals('new', $data['action']);
		
		// no segundo teste, verifica se o validator retorna uma mensagem por não encontrar o id na lista de roles
		$form->setData(array(
			'role' => 20,
			'resource' => 30,
			'action' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec orci nibh, faucibus ac tortor vel, porta ornare leo. Nunc ante quam, egestas in dui at, ultricies ultricies risus. Sed volutpat pretium leo sed viverra. Donec venenatis feugiat lorem, ut auctor dui varius vitae. Quisque venenatis eros ligula, ac pretium augue interdum vitae. Proin hendrerit nisi ac turpis ultricies, sit amet lacinia lacus ornare'
		));
		if ($form->isValid()) {
			$data = $form->getData();
		}

		$this->assertArrayHasKey('role', $form->getMessages());
		$this->assertArrayHasKey('resource', $form->getMessages());
		$this->assertArrayHasKey('action', $form->getMessages());
	}

}

