<?php

namespace Uacl\Form;

use TestBase\Test\TestCase;

class MenuTest extends TestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testVerificaSeDadosEnviadosParaOFormSaoFiltradosEValidados() {

		// inserindo um RESOURCE para tetar o filter que testa a existência da classe
		$resource = new \Uacl\Entity\Resource;
		$resource->setName('Jack\Controller\User')
				->setShortName('user')
				->setModule('jacl');
		$this->getEm()->persist($resource);

		// inserindo um menu para testes do PARENT
		$menu = new \Uacl\Entity\Menu;
		$menu->setName('Home')
				->setRoute('default-route')
				->setAction('index')
				->setDescription('Bem-vindo à Reduza')
				->setPosition(1)
				->setVisible(true)
				->setClass('icon-home')
				->setParent(null)
				->setResource($resource);
		$this->getEm()->persist($resource);

		$this->getEm()->flush();


		$form = new \Uacl\Form\Menu($this->getEm());

		// no primeiro teste, verifica se o filter stripTags está funcionando
		// o primeiro teste verifica se consegue inserir tendo o PARENT como null
		$form->setData(array(
			'name' => 'Usuários',
			'route' => 'default-route',
			'action' => 'index',
			'description' => 'Este é o menu de <b>Usuários</b>',
			'position' => 1,
			'visible' => true,
			'class' => 'icon-user',
			'parent' => null,
			'resource' => 1,
		));

		if ($form->isValid()) {
			$data = $form->getData();
		}
		
		$this->assertEquals('Este é o menu de Usuários', $data['description']);

		// no primeiro teste, verifica se o filter stripTags está funcionando
		// o primeiro teste verifica se consegue inserir tendo o PARENT como null
		$form->setData(array(
			'name' => 'Usuários',
			'route' => 'default-route',
			'action' => ' index ',
			'description' => 'Este é o menu de <b>Usuários</b>',
			'position' => 1,
			'visible' => true,
			'class' => 'icon-user',
			'parent' => 1,
			'resource' => 1,
		));

		if ($form->isValid()) {
			$data = $form->getData();
		}
		$this->assertEquals('index', $data['action']);

		// no segundo teste, verifica se o validator retorna uma mensagem por não encontrar o id na lista de roles
		$form->setData(array(
			'name' => 'Usuários',
			'route' => 'default-route',
			'action' => ' index ',
			'description' => 'Este é o menu de <b>Usuários</b>',
			'position' => 1,
			'visible' => true,
			'class' => 'icon-user',
			'parent' => null,
			'resource' => null,
		));
		if ($form->isValid()) {
			$data = $form->getData();
		}

		$this->assertArrayHasKey('resource', $form->getMessages());
	}

}

