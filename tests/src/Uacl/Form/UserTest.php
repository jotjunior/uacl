<?php

namespace Uacl\Form;

use TestBase\Test\TestCase;

class UserTest extends TestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testVerificaSeDadosEnviadosParaOFormSaoFiltradosEValidados() {

		// inserindo um ROLE para tetar o filter que testa a existência da classe
		$role = new \Uacl\Entity\Role;
		$role->setName('guest')
				->setLabel('Visitante')
				->setParent(null)
				->setAdmin(false);
		$this->getEm()->persist($role);

		// inserindo uma LANGUAGE para tetar o filter que testa a existência da classe
		$language = new \Uacl\Entity\Language;
		$language->setName('Português')
				->setNativeName('Português')
				->setLocale('pt_BR');
		$this->getEm()->persist($language);

		$this->getEm()->flush();


		$form = new \Uacl\Form\User($this->getEm());

		$form->setData(array(
			'firstName' => 'Marieta',
			'lastName' => 'Severo <script>alert("Oi!");</script>',
			'email' => 'marieta@severo.com',
			'emailConfirmation' => 'marieta@severo.com',
			'phone' => '(11) 4444-2222',
			'password' => '123456',
			'passwordConfirmation' => '123456',
			'active' => true,
			'language' => 1,
			'grantTypes' => 'password',
			'role' => 1,
		));

		if ($form->isValid()) {
			$data = $form->getData();
		} else {
			var_dump($form->getMessages());
		}
		$this->assertEquals('Severo alert("Oi!");', $data['lastName']);
		
		
		$form->setData(array(
			'firstName' => 'Marieta',
			'firstName' => 'Severo <script>alert("Oi!");</script>',
			'email' => 'marietasevero.com',
			'emailConfirmation' => 'marieta@severo.com',
			'phone' => '(11) 4444-2222',
			'password' => '12346',
			'passwordConfirmation' => '12346',
			'active' => true,
			'language' => 1,
		));

		if ($form->isValid()) {
			$data = $form->getData();
		}		
		// aviso dizendo que o password possui menos de 6 caracteres
		$this->assertArrayHasKey('password', $form->getMessages());
		// aviso que e-mail nao esta no formato correto
		$this->assertArrayHasKey('email', $form->getMessages());
		// aviso que nao foi enviado uma role
		$this->assertArrayHasKey('role', $form->getMessages());
		
	}

}

