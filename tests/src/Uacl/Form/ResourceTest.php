<?php

namespace Uacl\Form;

use TestBase\Test\TestCase;

class ResourceTest extends TestCase {

	/**
	 * Nome do módulo que está sendo testado no momento
	 * @var string
	 */
	protected $module = 'Uacl';
	
	public function testVerificaSeDadosEnviadosParaOFormSaoFiltradosEValidados() {
		
		$form = new \Uacl\Form\Resource();
		
		// testa o envio de texto com tags HTML e com espaços em branco no início e no final
		$form->setData(array( 
			'name' => 'Uacl\Controller\Resource',
			'shortName' => '<a>resource</a>',
			'module' => ' jacl '
		));
		
		if($form->isValid()) {
			$data = $form->getData();
		}

		$this->assertEquals('resource', $data['shortName']);
		$this->assertEquals('jacl', $data['module']);
		
		
		// testa o retorno de mensagem do formulário para textos maiores do que o limit permitido
		$form->setData(array( 
			'name' => 'Uacl\Controller\Resource',
			'shortName' => '<a>resource</a>',
			'module' => ' jacl askjdf ;alkdsjfa;kldfjsa;k ldsjfa;kkdfja ;dklsjf a;dskljf a;klsd jfa;kldsfjlsdjf a;klsdjf a;kdlsfj a;dsklfj a;dsklfj a;sdlkfj a;dsklfj a;ldfjs;kaljsdf;k'
		));
		
		if($form->isValid()) {
			$data = $form->getData();
		}
		
		$this->assertArrayHasKey('module', $form->getMessages());
	}

}

