<?php

namespace Uacl\Auth\Storage;

use TestBase\Test\TestCase;

class UserCredentialsStorageTest extends TestCase {

	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testTestaMetodoCheckUserCredentials() {
		$this->insertData();

		$storage = new \Uacl\Auth\Storage\UserCredentialsStorage($this->getEm());

		$check = $storage->checkUserCredentials('jot@jot.com.br', '123456');
		$this->assertTrue($check);
		
		// testando com senha incorreta
		$checkFalse = $storage->checkUserCredentials('jot@jot.com.br', '654321');
		$this->assertFalse($checkFalse);
		
		// testando com usuário inexistente
		$checkFalse = $storage->checkUserCredentials('naoexiste@jot.com.br', '654321');
		$this->assertFalse($checkFalse);
	}

	public function testTestaExibicaoDosDadosDoUsuario() {
		$this->insertData();

		$storage = new \Uacl\Auth\Storage\UserCredentialsStorage($this->getEm());
		$user = $storage->getUserDetails('jot@jot.com.br');

		$this->assertEquals('Jot', $user['first_name']);
	}

	protected function insertData() {

		$role = new \Uacl\Entity\Role;
		$role->setAdmin(false)
			->setLabel('Visitante')
			->setName('guest');
		$this->em->persist($role);

		$language = new \Uacl\Entity\Language;
		$language->setLocale('pt_BR')
			->setName('Português')
			->setNativeName('Português');
		$this->em->persist($language);

		$user = new \Uacl\Entity\User;
		$user->setRole($role)
			->setLanguage($language)
			->setFirstName('Jot')
			->setLastName('Junior')
			->setGrantTypes('password')
			->setPassword('123456')
			->setEmail('jot@jot.com.br')
			->setPhone('(27) 1234-5678')
			->setActive(true)
			->setActivationKey(md5('caterpillar'));
		$this->em->persist($user);

		$this->em->flush();
	}

}
