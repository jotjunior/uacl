<?php

namespace Uacl\Auth\Storage;

use TestBase\Test\TestCase;

class AccessTokenStorageTest extends TestCase {
	
	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testTestaInsercaoDoAccessToken() {
		$this->insertData();
		
		$storage = new \Uacl\Auth\Storage\AccessTokenStorage($this->getEm());
		
		$accessToken = $storage->setAccessToken(md5('jot2'), 'abcdefgh', null, time());
		$this->assertEquals(md5('jot2'), $accessToken['access_token']);
	}

	public function testTestaExibicaoDoAccessToken() {
		$this->insertData();
		$storage = new \Uacl\Auth\Storage\AccessTokenStorage($this->getEm());
		
		$accessToken = $storage->getAccessToken(md5('jot'));
		$this->assertEquals(md5('jot'), $accessToken['access_token']);

		$accessToken = $storage->getAccessToken(md5('jot3'));
		$this->assertFalse($accessToken);
	}

	protected function insertData() {
		$client = new \Uacl\Entity\Client;
		
		$client->setClientKey('abcdefgh')
			->setClientSecret(md5('abcdefgh'))
			->setGrantTypes('password')
			->setRedirectUri('http://epal.com.br');
		$this->em->persist($client);
		$this->em->flush();

		$storage = new \Uacl\Auth\Storage\AccessTokenStorage($this->getEm());
		$accessToken = $storage->setAccessToken(md5('jot'), 'abcdefgh', null, time());
	}

}
