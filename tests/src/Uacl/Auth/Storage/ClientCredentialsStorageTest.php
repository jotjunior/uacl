<?php

namespace Uacl\Auth\Storage;

use TestBase\Test\TestCase;

class ClientCredentialsStorageTest extends TestCase {

	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testTestaMetodoCheckClientCredentials() {
		$this->insertData();

		$storage = new \Uacl\Auth\Storage\ClientCredentialsStorage($this->getEm());

		$check = $storage->checkClientCredentials('abcdefgh', md5('abcdefgh'));
		$this->assertTrue($check);

		$checkFalse = $storage->checkClientCredentials('abcdefghij', md5('abcdefghij'));
		$this->assertFalse($checkFalse);
	}

	public function testTestaExibicaoDosDadosDoCliente() {
		$this->insertData();

		$storage = new \Uacl\Auth\Storage\ClientCredentialsStorage($this->getEm());
		$client = $storage->getClientDetails('abcdefgh');

		$this->assertEquals('password', $client['grant_types']);
	}

	public function testTestaChecagemDosGrantTypes() {
		$this->insertData();

		$storage = new \Uacl\Auth\Storage\ClientCredentialsStorage($this->getEm());
		
		$grantTrue = $storage->checkRestrictedGrantType('abcdefgh', 'password');
		$this->assertTrue($grantTrue);
		
		$grantFalse = $storage->checkRestrictedGrantType('abcdefgh', 'user_credentials');
		$this->assertFalse($grantFalse);
		
		$emptyGrant = $storage->checkRestrictedGrantType('abcdefghi');
		$this->assertTrue($emptyGrant);
		
	}
	
	public function testTestaSeEhUmPublicClient() {
		$storage = new \Uacl\Auth\Storage\ClientCredentialsStorage($this->getEm());
		$this->assertFalse($storage->isPublicClient('abcdefgh'));
	}

	protected function insertData() {

		$client = new \Uacl\Entity\Client;
		$client->setClientKey('abcdefgh')
			->setClientSecret(md5('abcdefgh'))
			->setGrantTypes('password')
			->setRedirectUri('http://epal.com.br');
		$this->em->persist($client);
		$this->em->flush();

		$client2 = new \Uacl\Entity\Client;
		$client2->setClientKey('abcdefghi')
			->setClientSecret(md5('abcdefghi'))
			->setGrantTypes('')
			->setRedirectUri('http://epal.com.br');
		$this->em->persist($client2);
		$this->em->flush();
	}

}
