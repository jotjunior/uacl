<?php

namespace Uacl\Auth\Storage;

use TestBase\Test\TestCase;

class AuthorizationCodeStorageTest extends TestCase {

	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testTestaInsercaoDoAuthorizationCode() {
		$this->insertData();
		$storage = new \Uacl\Auth\Storage\AuthorizationCodeStorage($this->getEm());
		$authorizationCode = $storage->setAuthorizationCode(md5('jot2'), 'abcdefgh', null, 'http://epal.com.br', time());
		$this->assertEquals(md5('jot2'), $authorizationCode['authorization_code']);
	}

	public function testTestaExibicaoDoAuthorizationCode() {
		$this->insertData();
		$storage = new \Uacl\Auth\Storage\AuthorizationCodeStorage($this->getEm());
		
		$authorizationCode = $storage->getAuthorizationCode(md5('jot'));
		$this->assertEquals(md5('jot'), $authorizationCode['authorization_code']);
		
		$authorizationCodeFalse = $storage->getAuthorizationCode(md5('jot3'));
		$this->assertFalse($authorizationCodeFalse);
	}

	public function testTestaExpiracaoDoAuthorizationCode() {
		$this->insertData();
		$storage = new \Uacl\Auth\Storage\AuthorizationCodeStorage($this->getEm());
		$storage->expireAuthorizationCode(md5('jot'));
		$data = $this->getEm()->getRepository('Uacl\Entity\AuthorizationCode')->find(md5('jot'));
		$this->assertNull($data);
	}

	protected function insertData() {

		$client = new \Uacl\Entity\Client;
		$client->setClientKey('abcdefgh')
			->setClientSecret(md5('abcdefgh'))
			->setGrantTypes('password')
			->setRedirectUri('http://epal.com.br');
		$this->em->persist($client);
		$this->em->flush();

		$storage = new \Uacl\Auth\Storage\AuthorizationCodeStorage($this->getEm());
		$storage->setAuthorizationCode(md5('jot'), 'abcdefgh', null, 'http://epal.com.br', time());
	}

}
