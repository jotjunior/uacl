<?php

namespace Uacl\Auth\Storage;

use TestBase\Test\TestCase;

class RefreshTokenStorageTest extends TestCase {

	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testTestaInsercaoDoRefreshToken() {
		$this->insertData();
		$storage = new \Uacl\Auth\Storage\RefreshTokenStorage($this->getEm());
		$refreshToken = $storage->setRefreshToken(md5('jot2'), 'abcdefgh', null, time());
		$this->assertEquals(md5('jot2'), $refreshToken['refresh_token']);
	}

	public function testTestaExibicaoDoRefreshToken() {
		$this->insertData();
		$storage = new \Uacl\Auth\Storage\RefreshTokenStorage($this->getEm());

		$refreshToken = $storage->getRefreshToken(md5('jot'));
		$this->assertEquals(md5('jot'), $refreshToken['refresh_token']);

		$refreshTokenFalse = $storage->getRefreshToken(md5('jot3'));
		$this->assertFalse($refreshTokenFalse);
	}

	public function testTestaExpiracaoDoRefreshToken() {
		$this->insertData();
		$storage = new \Uacl\Auth\Storage\RefreshTokenStorage($this->getEm());
		$storage->unsetRefreshToken(md5('jot'));
		$data = $this->getEm()->getRepository('Uacl\Entity\RefreshToken')->find(md5('jot'));
		$this->assertNull($data);
	}

	protected function insertData() {

		$client = new \Uacl\Entity\Client;
		$client->setClientKey('abcdefgh')
			->setClientSecret(md5('abcdefgh'))
			->setGrantTypes('password')
			->setRedirectUri('http://epal.com.br');
		$this->em->persist($client);
		$this->em->flush();

		$storage = new \Uacl\Auth\Storage\RefreshTokenStorage($this->getEm());
		$storage->setRefreshToken(md5('jot'), 'abcdefgh', null, time());
	}

}
