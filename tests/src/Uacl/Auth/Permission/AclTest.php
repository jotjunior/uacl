<?php

namespace Uacl\Auth\Permission;

use TestBase\Test\TestCase;

class AclTest extends TestCase {

	protected $module = 'Uacl';
	protected $requireDb = true;

	public function testTestaSePermissoesEstaoSendoDadas() {
		$this->insertData();

		// testes sem cache
		$acl = new \Uacl\Auth\Permission\Acl($this->em);
		$permission = $acl->isAllowed('guest', 'Uacl\Controller\Resource', 'getList');
		$this->assertTrue($permission);

		// testes com cache
		$cache = $this->serviceManager->get('Cache');

		if ($cache) {

			if ($cache->hasItem('uacl.roles.list')) {
				$cache->removeItem('uacl.roles.list');
			}

			if ($cache->hasItem('uacl.resources.list')) {
				$cache->removeItem('uacl.resources.list');
			}

			if ($cache->hasItem('uacl.privileges.list')) {
				$cache->removeItem('uacl.privileges.list');
			}

			$aclCache = new \Uacl\Auth\Permission\Acl($this->em, $cache);
			$permissionCached = $aclCache->isAllowed('guest', 'Uacl\Controller\Resource', 'getList');
			$this->assertTrue($permissionCached);
		}
	}

	protected function insertData() {

		$role = new \Uacl\Entity\Role;
		$role->setAdmin(false)
			->setLabel('Visitante')
			->setName('guest');
		$this->em->persist($role);
		$this->em->flush();

		$roleAdmin = new \Uacl\Entity\Role;
		$roleAdmin->setAdmin(true)
			->setLabel('Administrador')
			->setName('admin');
		$this->em->persist($roleAdmin);
		$this->em->flush();

		$resource = new \Uacl\Entity\Resource;
		$resource->setModule('uacl')
			->setName('Uacl\Controller\Resource')
			->setShortName('resource');
		$this->em->persist($resource);
		$this->em->flush();

		$privilege = new \Uacl\Entity\Privilege;
		$privilege->setResource($resource)
			->setRole($role)
			->setAction('getList');
		$this->em->persist($privilege);
		$this->em->flush();
	}

}
