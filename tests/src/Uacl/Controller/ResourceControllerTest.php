<?php

namespace Uacl\Controller;

use TestBase\Test\ControllerTestCase;
use Zend\Http\Request;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class ResourceControllerTest extends ControllerTestCase {

	protected $module = 'Uacl';
	protected $controllerFQDN = 'Uacl\Controller\ResourceController';
	protected $controllerRoute = 'api-route';
	protected $requireDb = true;

	public function testVerificaErro404() {
		$this->routeMatch->setParam('action', 'ActionNaoExiste');
		$this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testTestaRestfulGetList() {
		$this->insertData();


		$this->request->setMethod(Request::METHOD_GET);

		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->ArrayHasKey('list', $var);
	}

	public function testTestaRestfulGet() {
		// inserindo os dados que devem ser listados na controladora
		$service = new \Uacl\Service\Resource($this->getEm(), new DoctrineObject($this->getEm()));
		$service->insert(array(
			'name' => 'Uacl\Controller\Resource',
			'shortName' => 'jacl',
			'module' => 'resource',
		));
		

		$this->routeMatch->setParam('id', 1);
		$this->request->setMethod(Request::METHOD_GET);
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->assertArrayHasKey('data', $var);
	}

	public function testTestaRestfulPostSuccess() {
		$this->request->setMethod(Request::METHOD_POST);
		$this->request->getPost()->set('name', 'Uacl\Controller\Role');
		$this->request->getPost()->set('shortName', 'role');
		$this->request->getPost()->set('module', 'jacl');
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->assertEquals(1, $var['success']);
		$this->assertArrayHasKey('data', $var);

		// testa se retornou um valor inserido no banco
		$this->assertEquals('jacl', $var['data']['module']);
	}

	public function testTestaRestfulPostError() {
		$this->request->setMethod(Request::METHOD_POST);
		$this->request->getPost()->set('name', 'Uacl\Controller\Role');
		$this->request->getPost()->set('shortName', 'role');
		$this->request->getPost()->set('module', 'jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl jacl ');
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('Zend\View\Model\JsonModel', $result);
		$var = $result->getVariables();

		$this->assertEquals(0, $var['success']);
		$this->assertArrayHasKey('success', $var);
		$this->assertArrayHasKey('module', $var['data']);
	}

	public function testTestaRestfulPutSuccess() {
		$this->insertData();

		$this->routeMatch->setParam('id', 1);
		$this->request->setMethod(Request::METHOD_PUT);
		// Dica importante: para enviar dados pelo método PUT basta enviar os 
		// dados no formato querystring no request content
		$this->request->setContent('name=Uacl\Controller\Role&shortName=jacl&module=role');
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->assertEquals(1, $var['success']);
		$this->assertArrayHasKey('data', $var);

		// testa se retornou um valor inserido no banco
		$this->assertEquals('Uacl\Controller\Role', $var['data']['name']);
	}

	public function testTestaRestfulPutError() {
		$this->insertData();

		$this->routeMatch->setParam('id', 1);
		$this->request->setMethod(Request::METHOD_PUT);
		// Dica importante: para enviar dados pelo método PUT basta enviar os 
		// dados no formato querystring no request content
		$this->request->setContent('name=Uacl\Controller\Role&shortName=jacljacljacljacljacljacljacljacljacljacljacljacljacljacljacljacljacljacljacl&module=role');
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();

		$this->assertEquals(0, $var['success']);
		$this->assertArrayHasKey('success', $var);
		$this->assertArrayHasKey('shortName', $var['data']);
	}

	public function testTestaRestfulDeleteSuccess() {
		$this->insertData();
		
		$this->routeMatch->setParam('id', 1);
		$this->request->setMethod(Request::METHOD_DELETE);
		$result = $this->controller->dispatch($this->request, $this->response);

		$var = $result->getVariables();

		$this->assertEquals(1, $var['success']);
		$this->assertArrayHasKey('id', $var);
		$this->assertEquals(1, $var['id']);
	}

	protected function insertData() {
		// inserindo os dados que devem ser listados na controladora
		$service = new \Uacl\Service\Resource($this->getEm(), new DoctrineObject($this->getEm()));
		$service->insert(array(
			'name' => 'Uacl\Controller\Resource',
			'shortName' => 'resource',
			'module' => 'jacl',
		));
		
		$role = new \Uacl\Entity\Role;
		$role->setAdmin(true)
			->setLabel('Visitante')
			->setName('admin');
		$this->em->persist($role);
		$this->em->flush();

		
	}

}
