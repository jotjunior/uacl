<?php

namespace Uacl\Controller;

use TestBase\Test\ControllerTestCase;
use Uacl\Controller\MenuControllerTest;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;
use Zend\View\Renderer\PhpRenderer;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class IndexControllerTest extends ControllerTestCase {

	protected $module = 'Uacl';
	protected $controllerFQDN = 'Uacl\Controller\IndexController';
	protected $controllerRoute = 'api-route';
	protected $requireDb = true;

	public function testVerificaErro404() {
		$this->routeMatch->setParam('action', 'ActionNaoExiste');
		$result = $this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testVerificaStatus200ParaInvalidToken() {
		$this->controllerRoute = 'invalid-token';
		$this->routeMatch->setParam('action', 'invalid-token');
		$result = $this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testVerificaStatus200ParaForbidden() {
		$this->controllerRoute = 'forbidden';
		$this->routeMatch->setParam('action', 'forbidden');
		$result = $this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testVerificaStatus200ParaInvalidResource() {
		$this->controllerRoute = 'invalid-resource';
		$this->routeMatch->setParam('action', 'invalid-resource');
		$result = $this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(200, $response->getStatusCode());
	}

}
