<?php

namespace Uacl\Controller;

use TestBase\Test\ControllerTestCase;
use Uacl\Controller\RoleControllerTest;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;
use Zend\View\Renderer\PhpRenderer;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class RoleControllerTest extends ControllerTestCase {

	protected $module = 'Uacl';
	protected $controllerFQDN = 'Uacl\Controller\RoleController';
	protected $controllerRoute = 'api-route';
	protected $requireDb = true;

	public function testVerificaErro404() {
		$this->routeMatch->setParam('action', 'ActionNaoExiste');
		$result = $this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testTestaRestfulGetList() {
		// inserindo os dados que devem ser listados na controladora
		$service = new \Uacl\Service\Role($this->getEm(), new DoctrineObject($this->getEm()));
		$guest = $service->insert(array(
			'name' => 'guest',
			'parent' => null,
			'admin' => false,
			'label' => 'Visitante'
		));
		$registrado = $service->insert(array(
			'name' => 'registered',
			'parent' => $guest,
			'admin' => false,
			'label' => 'Registrado'
		));

		$this->request->setMethod(Request::METHOD_GET);

		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);
		
		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->ArrayHasKey('list', $var);
		
	}

	public function testTestaRestfulGet() {
		// inserindo os dados que devem ser listados na controladora
		$service = new \Uacl\Service\Role($this->getEm(), new DoctrineObject($this->getEm()));
		$guest = $service->insert(array(
			'name' => 'guest',
			'parent' => null,
			'admin' => false,
			'label' => 'Visitante'
		));

		$this->request->setMethod(Request::METHOD_GET);
		$this->routeMatch->setParam('id', 1);

		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);
		
		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->ArrayHasKey('data', $var);
		
	}

}