<?php

namespace Uacl\Controller;

use TestBase\Test\ControllerTestCase;
use Uacl\Controller\MenuControllerTest;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;
use Zend\View\Renderer\PhpRenderer;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class MenuControllerTest extends ControllerTestCase {

	protected $module = 'Uacl';
	protected $controllerFQDN = 'Uacl\Controller\MenuController';
	protected $controllerRoute = 'api-route';
	protected $requireDb = true;

	public function testVerificaErro404() {
		$this->routeMatch->setParam('action', 'ActionNaoExiste');
		$result = $this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testTestaRestfulGetList() {
		$this->insertData();

		$this->request->setMethod(Request::METHOD_GET);

		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->ArrayHasKey('list', $var);
	}

	public function testTestaRestfulGet() {
		$this->insertData();

		$this->request->setMethod(Request::METHOD_GET);
		$this->routeMatch->setParam('id', 1);

		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->ArrayHasKey('data', $var);
	}

	public function testTestaRestfulPostSuccess() {
		$this->insertData();

		$this->request->setMethod(Request::METHOD_POST);
		$this->request->getPost()->set('resource', 1);
		$this->request->getPost()->set('name', 'Home');
		$this->request->getPost()->set('route', 'default');
		$this->request->getPost()->set('action', 'index');
		$this->request->getPost()->set('description', 'Página Principal');
		$this->request->getPost()->set('position', 2);
		$this->request->getPost()->set('visible', true);
		$this->request->getPost()->set('class', 'icon-home');
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->assertEquals(1, $var['success']);
		$this->assertArrayHasKey('data', $var);

		// testa se retornou um valor inserido no banco
		$this->assertEquals('Home', $var['data']['name']);
	}

	public function testTestaRestfulPostError() {
		$this->request->setMethod(Request::METHOD_POST);
		$this->request->getPost()->set('resource', 1);
		$this->request->getPost()->set('name', 'Home');
		$this->request->getPost()->set('route', 'default');
		$this->request->getPost()->set('action', 'index');
		$this->request->getPost()->set('description', 'Página Principal');
		$this->request->getPost()->set('position', 2);
		$this->request->getPost()->set('visible', true);
		$this->request->getPost()->set('class', 'icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home');
		$result = $this->controller->dispatch($this->request, $this->response);

		$var = $result->getVariables();

		$this->assertEquals(0, $var['success']);
		$this->assertArrayHasKey('class', $var['data']);
	}

	public function testTestaRestfulPutSuccess() {
		$this->insertData();

		$this->routeMatch->setParam('id', 1);
		$this->request->setMethod(Request::METHOD_PUT);
		// Dica importante: para enviar dados pelo método PUT basta enviar os 
		// dados no formato querystring no request content
		$this->request->setContent(http_build_query(array(
			'resource' => 1,
			'name' => 'Papéis',
			'route' => 'default',
			'action' => 'index',
			'description' => 'Lista de papéis dos usuários',
			'position' => 1,
			'visible' => true,
			'class' => 'icon-users',
		)));
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->assertEquals(1, $var['success']);
		$this->assertArrayHasKey('data', $var);

		// testa se retornou um valor inserido no banco
		$this->assertEquals('icon-users', $var['data']['class']);
	}

	public function testTestaRestfulPutError() {
		$this->insertData();

		$this->routeMatch->setParam('id', 1);
		$this->request->setMethod(Request::METHOD_PUT);
		// Dica importante: para enviar dados pelo método PUT basta enviar os 
		// dados no formato querystring no request content
		$this->request->setContent(http_build_query(array(
			'resource' => 1,
			'name' => 'Papéis',
			'route' => 'default',
			'action' => 'index',
			'description' => 'Lista de papéis dos usuários',
			'position' => 1,
			'visible' => true,
			'class' => 'icon-users icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home icon-home',
		)));
		$result = $this->controller->dispatch($this->request, $this->response);

		$var = $result->getVariables();

		$this->assertEquals(0, $var['success']);
		$this->assertArrayHasKey('class', $var['data']);
	}

	public function testTestaRestfulDeleteSuccess() {
		$this->insertData();

		$this->routeMatch->setParam('id', 1);
		$this->request->setMethod(Request::METHOD_DELETE);
		$result = $this->controller->dispatch($this->request, $this->response);

		$var = $result->getVariables();

		$this->assertEquals(1, $var['success']);
		$this->assertArrayHasKey('id', $var);
		$this->assertEquals(1, $var['id']);
	}

	/**
	 * Inserindo os dados que serão testados pelos métodos
	 */
	protected function insertData() {
		// inserindo os dados que devem ser listados na controladora
		$serviceResource = new \Uacl\Service\Resource($this->getEm(), new DoctrineObject($this->getEm()));
		$resource = $serviceResource->insert(array(
			'name' => 'Uacl\Controller\Role',
			'shortName' => 'role',
			'module' => 'uacl',
		));

		$serviceMenu = new \Uacl\Service\Menu($this->getEm(), new DoctrineObject($this->getEm()));
		$menu = $serviceMenu->insert(array(
			'resource' => $resource,
			'name' => 'Papéis',
			'route' => 'default',
			'action' => 'index',
			'description' => 'Lista de papéis dos usuários',
			'position' => 1,
			'visible' => true,
			'class' => 'icon-user',
		));
	}

}
