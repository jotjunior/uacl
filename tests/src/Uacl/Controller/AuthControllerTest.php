<?php

namespace Uacl\Controller;

use TestBase\Test\ControllerTestCase;
use Uacl\Controller\UserControllerTest;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;
use Zend\View\Renderer\PhpRenderer;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\Mvc\Router\RouteMatch;

class AuthControllerTest extends ControllerTestCase {

	protected $module = 'Uacl';
	protected $controllerFQDN = 'Uacl\Controller\AuthController';
	protected $controllerRoute = 'authprovider';
	protected $requireDb = true;

	public function testVerificaErro404() {
		$this->routeMatch->setParam('action', 'ActionNaoExiste');
		$result = $this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testTestaAuthorizeAction() {
		$this->routeMatch->setParam('action', 'authorize');
		$this->request->setMethod(Request::METHOD_GET);
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->ArrayHasKey('error', $var);
	}

	public function testTestaRequestAction() {
		$this->routeMatch->setParam('action', 'request');
		$this->request->setMethod(Request::METHOD_POST);
		$this->request->getPost()->set('username', 'jot@jot.com.br');
		$this->request->getPost()->set('password', '123456');
		$this->request->getPost()->set('client_id', 'abcdefgh');
		$this->request->getPost()->set('client_secret', md5('abcdefgh'));
		$this->request->getPost()->set('grant_type', 'password');
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);

	}

	public function testTestaResourceAction() {
		$this->insertData();

		$token = new \Uacl\Auth\Storage\AccessTokenStorage($this->getEm());
		$accessToken = $token->setAccessToken(md5('jot'), 'abcdefgh', 'jot@jot.com.br', time() + 3600);

		$this->routeMatch->setParam('action', 'resource');
		$this->request->setMethod(Request::METHOD_GET);
		$this->request->getQuery()->set('access_token', $accessToken['access_token']);
		$result = $this->controller->dispatch($this->request, $this->response);

		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);

	}

	protected function insertData() {

		$client = new \Uacl\Entity\Client;
		$client->setClientKey('abcdefgh')
			->setClientSecret(md5('abcdefgh'))
			->setGrantTypes('password')
			->setRedirectUri('http://epal.com.br');
		$this->em->persist($client);
		$this->em->flush();

		$role = new \Uacl\Entity\Role;
		$role->setAdmin(false)
			->setLabel('Visitante')
			->setName('guest');
		$this->em->persist($role);

		$language = new \Uacl\Entity\Language;
		$language->setLocale('pt_BR')
			->setName('Português')
			->setNativeName('Português');
		$this->em->persist($language);

		$user = new \Uacl\Entity\User;
		$user->setRole($role)
			->setLanguage($language)
			->setFirstName('Jot')
			->setLastName('Junior')
			->setGrantTypes('password')
			->setPassword('123456')
			->setEmail('jot@jot.com.br')
			->setPhone('(27) 1234-5678')
			->setActive(true)
			->setActivationKey(md5('caterpillar'));
		$this->em->persist($user);

		$this->em->flush();
	}

}
