<?php

namespace Uacl\Controller;

use TestBase\Test\ControllerTestCase;
use Zend\Http\Request;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;

class ResetPrivilegesControllerTest extends ControllerTestCase {

	protected $module = 'Uacl';
	protected $controllerFQDN = 'Uacl\Controller\ResetPrivilegesController';
	protected $controllerRoute = 'api-route';
	protected $requireDb = true;

	public function testVerificaErro404() {
		$this->routeMatch->setParam('action', 'ActionNaoExiste');
		$this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testTestaRestfulGetList() {
		$this->insertData();

		$this->request->setMethod(Request::METHOD_GET);
		$result = $this->controller->dispatch($this->request, $this->response);

		// testa se está retornando um JsonModel
		$this->assertInstanceOf('\Zend\View\Model\JsonModel', $result);

		// testa se há um array injetado no JsonModel
		$var = $result->getVariables();
		$this->ArrayHasKey('list', $var);
	}

	/**
	 * @expectedException Exception
	 */
	public function testTestaRestfulGetListError() {
		// inserindo os dados que devem ser listados na controladora
		$service = new \Uacl\Service\Resource($this->getEm(), new DoctrineObject($this->getEm()));
		$service->insert(array(
			'name' => 'Uacl\Controller\Resource',
			'shortName' => 'resource',
			'module' => 'jacl',
		));

		$this->request->setMethod(Request::METHOD_GET);
		$result = $this->controller->dispatch($this->request, $this->response);

	}

	protected function insertData() {
		// inserindo os dados que devem ser listados na controladora
		$service = new \Uacl\Service\Resource($this->getEm(), new DoctrineObject($this->getEm()));
		$service->insert(array(
			'name' => 'Uacl\Controller\Resource',
			'shortName' => 'resource',
			'module' => 'jacl',
		));

		$role = new \Uacl\Entity\Role;
		$role->setAdmin(true)
			->setLabel('Visitante')
			->setName('admin');
		$this->em->persist($role);
		$this->em->flush();
	}

}
