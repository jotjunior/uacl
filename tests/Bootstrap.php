<?php

namespace Uacl;

require_once ('../../TestBase/src/TestBase/Test/AbstractBootstrap.php');

use TestBase\Test\AbstractBootstrap;

// forçando exibição de erros, para uso do phpunit
error_reporting(E_ALL | E_STRICT);

// entrando no diretório do arquivo
chdir(__DIR__);

class Bootstrap extends AbstractBootstrap {
	
}

Bootstrap::init();