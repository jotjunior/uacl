<?php

namespace Uacl;

return array(
    'router' => array(
        'routes' => array(
            'api-route' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/uacl/[:controller[/:id]]',
                    'constraints' => array(
                        'id' => '[a-zA-Z0-9_-]+',
                        'controller' => '[a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Uacl\Controller',
                        'controller' => 'Index',
                    )
                )
            ),
            'create-admin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/create-admin',
                    'constraints' => array(
                        'controller' => '[a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Uacl\Controller',
                        'controller' => 'Index',
                        'action' => 'create-admin',
                    )
                )
            ),
            'invalid-token' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/invalid-token',
                    'constraints' => array(
                        'controller' => '[a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Uacl\Controller',
                        'controller' => 'Index',
                        'action' => 'invalid-token',
                    )
                )
            ),
            'invalid-resource' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/invalid-resource',
                    'constraints' => array(
                        'controller' => '[a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Uacl\Controller',
                        'controller' => 'Index',
                        'action' => 'invalid-resource',
                    )
                )
            ),
            'forbidden' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/forbidden',
                    'constraints' => array(
                        'controller' => '[a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Uacl\Controller',
                        'controller' => 'Index',
                        'action' => 'forbidden',
                    )
                )
            ),
            'authprovider' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/auth',
                    'constraints' => array(),
                    'defaults' => array(
                        'controller' => 'Uacl\Controller\Auth',
                        'action' => 'index',
                    ),
                ),
                'child_routes' => array(
                    'version' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[/:version]',
                            'constraints' => array(
                                'version' => 'v[a-zA-Z0-9._-]+',
                            ),
                            'defaults' => array(
                                'version' => null
                            ),
                        ),
                        'child_routes' => array(
                            'request_token' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/request',
                                    'defaults' => array(
                                        'controller' => 'Uacl\Controller\Auth',
                                        'action' => 'request',
                                    ),
                                ),
                            ),
                            'authorize' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/authorize',
                                    'defaults' => array(
                                        'controller' => 'Uacl\Controller\Auth',
                                        'action' => 'authorize',
                                    ),
                                ),
                            ),
                            'access_token' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/resource',
                                    'defaults' => array(
                                        'controller' => 'Uacl\Controller\Auth',
                                        'action' => 'resource',
                                    ),
                                ),
                            ),
                            'user' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/user',
                                    'defaults' => array(
                                        'controller' => 'Uacl\Controller\Auth',
                                        'action' => 'user',
                                    ),
                                ),
                            ),
                            'token' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/token',
                                    'defaults' => array(
                                        'controller' => 'Uacl\Controller\Auth',
                                        'action' => 'token',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            'api-doc' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/doc/[:controller]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                        'controller' => '[a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Uacl\Controller',
                        'controller' => 'Index',
                        'action' => 'documentation'
                    )
                )
            ),
            
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Uacl\Controller\Auth' => 'Uacl\Controller\AuthController',
            'Uacl\Controller\Index' => 'Uacl\Controller\IndexController',
            'Uacl\Controller\Role' => 'Uacl\Controller\RoleController',
            'Uacl\Controller\Resource' => 'Uacl\Controller\ResourceController',
            'Uacl\Controller\Privilege' => 'Uacl\Controller\PrivilegeController',
            'Uacl\Controller\ResetPrivileges' => 'Uacl\Controller\ResetPrivilegesController',
            'Uacl\Controller\Menu' => 'Uacl\Controller\MenuController',
            'Uacl\Controller\User' => 'Uacl\Controller\UserController',
            'Uacl\Controller\Language' => 'Uacl\Controller\LanguageController',
            'Uacl\Controller\ResetPassword' => 'Uacl\Controller\ResetPasswordController',
            'Uacl\Controller\NewPassword' => 'Uacl\Controller\NewPasswordController',
            'Uacl\Controller\Validate' => 'Uacl\Controller\ValidateController',
            'Uacl\Controller\ChangeRole' => 'Uacl\Controller\ChangeRoleController',
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy'
        ),
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'doc'                     => __DIR__ . '../../view/documentation/documentation.phtml'
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),    
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ),
                'cache' => array(
                    'instance' => 'Cache'
                )
            ),
        ),
        'cache' => array(
            'instance' => 'Cache'
        )
    ),
    'oauth2provider' => array(
        'servers' => array(
            'default' => array(
                'storages' => array(
                    'user_credentials' => 'Uacl\UserCredentials',
                    'access_token' => 'Uacl\AccessToken',
                    'client_credentials' => 'Uacl\ClientCredentials',
                    'authorization_code' => 'Uacl\AuthorizationCode',
                    'refresh_token' => 'Uacl\RefreshToken',
                ),
                'configs' => array(
                    'access_lifetime' => 43200,
                    'www_realm' => 'Service',
                    'token_param_name' => 'access_token',
                    'token_bearer_header_name' => 'Bearer',
                    'enforce_state' => true,
                    'require_exact_redirect_uri' => true,
                    'allow_implicit' => false,
                    'allow_credentials_in_request_body' => true,
                ),
                'server_class' => 'OAuth2Provider\Server',
                'version' => 'v1',
                'controller' => 'Uacl\Controller\AuthController',
                'response_types' => array(
                    array(
                        'name' => 'access_token',
                        'options' => array(
                            'configs' => array(
                                'token_type' => 'bearer',
                                'access_lifetime' => 43200,
                                'refresh_token_lifetime' => 1209600,
                            ),
                        ),
                    ),
                    array(
                        'name' => 'authorization_code',
                        'options' => array(
                            'configs' => array(
                                'enforce_redirect' => false,
                                'auth_code_lifetime' => 30,
                            ),
                        ),
                    ),
                ),
                'token_type' => array(
                    'name' => 'bearer',
                    'options' => array(
                        'configs' => array(
                            'token_param_name' => 'access_token',
                            'token_bearer_header_name' => 'Bearer',
                        ),
                    ),
                ),
                'client_assertion_type' => array(
                    'name' => 'http_basic',
                    'options' => array(
                        'configs' => array(
                            'allow_credentials_in_request_body' => true
                        ),
                    ),
                ),
            ),
        ),
        'main_server' => 'default',
        'main_version' => 'v1',
        'default_controller' => 'Uacl\Controller\AuthController',
    )
);
